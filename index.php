<?php
    // error_reporting(E_ALL ^ E_NOTICE);
	// THIS SHOULD BE CHANGED
	ini_set('post_max_size', '10M');
	ini_set('upload_max_filesize', '10M');

	error_reporting(0);
	$domain = parse_url($_SERVER['HTTP_ORIGIN']);
	$FQD = $domain['scheme']."://".$domain['host'];
	if(isset($domain['port']) && $domain['port'] != 80){
		$FQD = $FQD.":".$domain['port'];
	}
	header('Access-Control-Allow-Origin: '.$FQD);
	header("Access-Control-Allow-Methods: GET, POST, OPTIONS, DELETE, PUT");
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Max-Age: 10');
	header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');

 if($_SERVER['REQUEST_METHOD'] == "OPTIONS"){
	exit;
}

/*IE8,9 CORS patch*/
if($_GET['_method']){
	$_SERVER['REQUEST_METHOD'] = $_GET['_method'];
}

/* Init KriekApps API */
require_once 'core/api.php';
$api = new Api();
$params = array();
$app_data = array();


/* Auth */
$api->slim->hook('slim.before.dispatch', function () use ($api) {
    $route = $api->slim->router()->getCurrentRoute();
    $params = $route->getParams();

    if(isset($params['app_id'])){
    	$app_id = $params['app_id'];
    } else if (is_numeric($params['url'][0])) {
    	$app_id = $params['url'][0];
    } else {
    	$app_id = null;
    }

 	// Do Auth
    $auth = new Auth($app_id);

    // Store App DATA
    if($app_id){
    	global $app_data;
    	$data = $api->memcache->get('app_data_'.$app_id);

		if(!isset($data) || $data == "") {
			$app = new App();
			$app_data = $app->getAppData($app_id);
			$api->setMemcacheValue('app_data_'.$app_id,$app_data);
		} else {
			$app_data = $data;
		}
    }


});



/* Routes */

	/* Get Client Data */
	$api->slim->get('/imgproxy', function() use ($api){
		$remoteImage = urldecode($_GET['img']);
		$imginfo = getimagesize($remoteImage);
		header("Content-type: ".$imginfo['mime']);
		readfile($remoteImage);
		die();
	});

	/* Get Access Token */
	$api->slim->post('/auth/login', function(){
		$auth = new Client_Auth();
		$auth->getAdminToken();
	});

	/* Get Client Data */
	$api->slim->get('/auth/me', function() use ($api){
		$auth = new Client_Auth();
		$client = $auth->validateAdminToken($_SESSION['admin_token']);
		$api->return_json(array("status" => "success", "data" => $client));
	});

/* FB PAGE PROXY */
	$api->slim->post('/:app_id/redirectToFacebookTab/', function($app_id) use ($api){
		$fb = new FB();
		$fb->redirectToFacebookTab();
	});

/* FB CANVAS PROXY */
	$api->slim->map('/:app_id/redirectFromCanvas/', function($app_id) use ($api){
		global $app_data;
		if(isset($_GET['redirectUrl'])){
			if(strpos($_GET['redirectUrl'], "?")){$con = "&";} else {$con = "?";}
			$url = $_GET['redirectUrl'].$con."notif_t=app_notification_retarget&";
		} else {
			$useragent=$_SERVER['HTTP_USER_AGENT'];
			if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))){
				$url = $app_data["config"]["public"]["app_url"]["value"].'#'.urlencode(json_encode(Array('app_data' => json_encode($_GET))));
			} else {
				$url = $app_data["config"]["public"]["app_fb_page_url"]["value"];
				if(strpos($url, "?")){$con = "&";} else {$con = "?";}
				$url = $url.$con.'app_data='.urlencode(json_encode($_GET));
			}
		}
		$_GET['timestamp'] = time();

		echo '<html>'
			.'<head>'
			.'<title>'.$app_data["name"].'</title>'
		    .'<meta property="og:title" content="'.$app_data["config"]["public"]["share_title"]["value"].'" />'
		    .'<meta property="og:type" content="article" />'
		    .'<meta property="og:image" content="'.$app_data["config"]["public"]["share_thumbnail_url"]["value"].'" />'
		    .'<meta property="og:description" content="'.$app_data["config"]["public"]["share_description"]["value"].'" />'
			.'</head>'
			.'<body>'
			.'<script>top.location = "'.$url.'";</script>'
			.'</body>'
			.'</html>';
	})->via('GET', 'POST');

	$api->slim->map('/:app_id/redirectFromCanvas/index.php', function($app_id) use ($api){
		global $app_data;
		if(isset($_GET['redirectUrl'])){
			if(strpos($_GET['redirectUrl'], "?")){$con = "&";} else {$con = "?";}
			$url = $_GET['redirectUrl'].$con."notif_t=app_notification_retarget&";
		} else {
			$useragent=$_SERVER['HTTP_USER_AGENT'];
			if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))){
				$url = $app_data["config"]["public"]["app_url"]["value"].'#'.urlencode(json_encode(Array('app_data' => json_encode($_GET))));
			} else {
				$url = $app_data["config"]["public"]["app_fb_page_url"]["value"];
				if(strpos($url, "?")){$con = "&";} else {$con = "?";}
				$url = $url.$con.'app_data='.urlencode(json_encode($_GET));
			}
		}
		$_GET['timestamp'] = time();

		echo '<html>'
			.'<head>'
			.'<title>'.$app_data["name"].'</title>'
		    .'<meta property="og:title" content="'.$app_data["config"]["public"]["share_title"]["value"].'" />'
		    .'<meta property="og:type" content="article" />'
		    .'<meta property="og:image" content="'.$app_data["config"]["public"]["share_thumbnail_url"]["value"].'" />'
		    .'<meta property="og:description" content="'.$app_data["config"]["public"]["share_description"]["value"].'" />'
			.'</head>'
			.'<body>'
			.'<script>top.location = "'.$url.'";</script>'
			.'</body>'
			.'</html>';
	})->via('GET', 'POST');

/* SQL Routes */
$api->slim->map('/:url+', function($url=null) use ($api){
	$end = new Endpoint();
	if(!$end->getRoute($url)) {
		$api->slim->pass();
	}
})->via('GET', 'POST','PUT','DELETE');

/**** Unknown Endpoint ****/
	$api->slim->notFound(function () use ($api) {
	    $api->return_error('Unknown endpoint');
	});

/* Start API */
$api->slim->run();

?>
