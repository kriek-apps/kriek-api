<?php
/* KriekApps API Tasks Module */

class Tasks extends Api{

	function runTasks(){
		$sql="SELECT a.*,b.process FROM tasks AS a LEFT JOIN tasks_templates AS b ON a.template=b.id WHERE active=1 AND NOW()-date>period*60";
		$data = $this->DB->runSQL($sql,null,'collection');

		foreach ($data as $key => $value) {
			$value['data'] = json_decode($value['data'],TRUE);
			$this->runTask($value);
			$sql="UPDATE tasks SET date=NOW() WHERE id=:id LIMIT 1";
			$this->DB->runSQL($sql,array("id"=>$value['id']));
		}
	}

	function runTask($task){
		$class = explode('::', $task['process']);
		if (!class_exists($class[0])) {
			$temp = "core/".strtolower($class[0]);
			if($class[0] == "Custom") {
				$temp = "core/custom/".$_SESSION['app_id'];
			}
			try {
				require_once $temp.'.php';
			} catch(exeption $e) {
				$this->return_error($e->getMessage());
			}
		}
		$app = new $class[0]();
		call_user_func(array($app, $class[1]), $task);
		//unset($app);
	}

	function updateTaskLog($SQLparams){
		$sql="INSERT INTO tasks_log (app_id,user_id,task,identifier,last_data) VALUES (:app_id,:user_id,:task,:identifier,:last_data)";
		$this->DB->runSQL($sql,$SQLparams);
	}

	function notifyUser($notifParams){
		//notification process jhjh
	}

	function votesNotification($task){
		$sql="SELECT a.id,a.user_id,a.votes AS data FROM 
		(SELECT id,user_id,votes FROM ugc_entries WHERE app_id=:app_id AND disabled!=1) AS a 
		LEFT JOIN 
		(SELECT MAX(last_data) AS last_data,identifier FROM tasks_log WHERE app_id=:app_id AND task=:task GROUP BY identifier) AS b 
		ON a.id=b.identifier
		WHERE a.votes-IFNULL(b.last_data,0)>:data=>vote_limit";

		$data = $this->DB->runSQL($sql,array("app_id"=>$task['app_id'],"task"=>$task['id'],"data"=>$task['data']),'collection');

		foreach ($data as $key => $value) {
			$this->updateTaskLog(array("app_id"=>$task['app_id'],"task"=>$task['id'],"identifier"=>$value["id"],"user_id"=>$value["user_id"],"last_data"=>$value["data"]));
		}

		$this->return_json($data);
	}

	function testMemcache(){
		$this->memcache->set('test',array('val1' => 1,'val2' => 2 ));
	}
}