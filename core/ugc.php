<?php
/* KriekApps API UGC Module */

//require_once 'app.php';

class Ugc extends Api{
//new
	function __construct(){
		parent::__construct();
		global $app_data;
		$this->appdata = $app_data;
		//$this->appdata = App::getAppData($_SESSION['app_id']);
	}

	function checkEntries(){
		global $params;
		global $app_data;
		$this->canIaddEntry();

		if(!isset($params['disabled'])) {
			$params['disabled'] = 0;
		}
		if($app_data['config']['public']['modules']['ugc']['moderation']) {
			$params['disabled'] = 1;
		}
	}

	function checkVotes(){
		if($this->canIvote() === false) {
			$this->return_json(array("status" => "error", "message" => "Already voted for this entry"));
			$this->slim->stop();
		}
	}

	function updateVoteCount(){
		$this->updateEntryVotecount();
	}

	function getUGC(){
		global $params;

		$url_param = "";

		$order = 'id';
		if($params['by'] == 'date') {
			$order = 'date';
			$url_param = '&by='.$order;
		} else if($params['by'] == 'votes') {
			$order = 'votes';
			$url_param = '&by='.$order;
		}

		$asc_desc = "DESC";
		if($params['order'] == "asc") {
			$asc_desc = "ASC";
			$url_param .= '&order=asc';
		}

		if(isset($params['seed'])) {
			$order = 'RAND(:seed)';
			$asc_desc = "";
			$url_param = '&seed='.$order;
		}

		$search = "";
		if(isset($params['search'])) {
			$url_param .= '&search='.$params['search'];
			$params['search'] = '%'.$params['search'].'%';
			$search = " AND (ugc_entries.data LIKE (:search) OR ugc_entries.date LIKE (:search) OR users.name LIKE (:search)) ";
		}

		if(!isset($params['round'])) {
			$params['round'] = 0;
		}

		if(!isset($params['category'])) {
			$params['category'] = '%';
		}

		$sql="SELECT ugc_entries.id,ugc_entries.user_id,users.name as user_name,ugc_entries.data,ugc_entries.votes,ugc_entries.category,ugc_entries.round FROM ugc_entries LEFT JOIN users ON ugc_entries.user_id = users.id AND ugc_entries.app_id = users.app_id WHERE ugc_entries.round=:round AND ugc_entries.category LIKE (:category) AND ugc_entries.app_id=:app_id AND disabled = 0 SEARCH ORDER BY ".$order." ".$asc_desc;
		$sql = str_replace("SEARCH",$search,$sql);

		if($search != "") {
			$data = $this->DB->runSQL($sql,null,"collection");
			$params['total'] = count($data);
		}

		if(!isset($params['total'])) {
			$all="SELECT COUNT(id) AS count FROM ugc_entries WHERE round=:round AND category LIKE (:category) AND app_id=:app_id AND disabled = 0";
			$alldata = $this->DB->runSQL($all,null,"model");
			$params['total'] = $alldata['count'];
		}

		if(isset($params['limit'])) {
			if(is_numeric($params['limit'])) {
				$sql .= " LIMIT ".abs((int)$params['limit'])." OFFSET ".abs((int)$params['offset']);
			}
		} else {
			$params['limit'] = 200;
			$sql .= " LIMIT 200";
		}

		$data = $this->DB->runSQL($sql,null,"collection");

		if((int)$params['limit'] + (int)$params['offset'] < (int)$params['total']) {
			$next = $params['app_id'].'/entries?total='.$params['total'].'&limit='.$params['limit'].'&offset='.((int)$params['offset']+(int)$params['limit']).$url_param;
		}

		if((int)$params['offset'] - (int)$params['limit'] >= 0) {
			$previous = $params['app_id'].'/entries?total='.$params['total'].'&limit='.$params['limit'].'&offset='.((int)$params['offset']-(int)$params['limit']).$url_param;
		}

		$extra=array(
			"totalResults"=>$params['total'],
			"previous"=>$previous,
			"next"=>$next
		);

		$params['return_data'] = $data;
		$this->convertToJSON(array("data"));

		$this->return_json(array_merge(array("results"=>$params['return_data']),$extra));
	}
//new
	function updateEntryVotecount(){
		$sql = "UPDATE ugc_entries SET votes = votes + 1 WHERE app_id=:app_id AND id=:entry_id";
		$this->DB->runSQL($sql);
	}

	function canIvote(){
			$sql = "SELECT count(*) AS voted FROM ugc_votes WHERE entry_id=:entry_id AND user_id=:user_id AND app_id=:app_id";
			$data = $this->DB->runSQL($sql,null,"model");
			if($data['voted'] == 0){
				return true;
			} else {
				return false;
			}
	}

	function canIaddEntry(){
		global $params;

		error_reporting(E_ALL ^ E_NOTICE);
		$category_count = 0;
		$my_categories = array();
		
		$sql = "SELECT * FROM ugc_entries WHERE user_id=:user_id AND app_id=:app_id AND round=:round AND disabled=false";
		$data = $this->DB->runSQL($sql,null,"collection");

		foreach($data as $row){
			$my_categories[$row['category']] = $my_categories[$row['category']] + 1;
		}

		// TODO BUG ezért kell mindig egyel kevesebbet megadni adminon <= nek kéne lennie
		if($this->appdata['config']['public']['modules']['ugc']['max_upload_per_user'] < count($data)){
			$this->return_error("max_upload_per_user");
		}

		if($this->appdata['config']['public']['modules']['ugc']['max_upload_per_user_per_category'] <= $my_categories[$params['category']]){
			$this->return_error("max_upload_per_user_per_category");
		}

		if(!array_key_exists($params['category'], $my_categories) && $this->appdata['config']['public']['modules']['ugc']['max_category_per_user'] <= count($my_categories)){
			$this->return_error("max_category_per_user");
		}

		return true;

	}

	function ugcMeta(){
		$sql = "SELECT * FROM ugc_entries WHERE id=:entry_id";
		$data = $this->DB->runSQL($sql,null,"model");
	
		$entry = json_decode($data['data'],TRUE);

		$res = $this->slim->getInstance()->response();
		$res['Content-Type'] = 'text/html';

		global $params;
		global $app_data;

		if(isset($entry['title'])) {
			$title = $entry['title'];
		} else {
			$title = $app_data['config']['public']['share_title']['value'];
		}

		if(isset($entry['description'])) {
			$description = $entry['description'];
		} else {
			$description = $app_data['config']['public']['share_description']['value'];
		}

		if(isset($entry['pictures']['original'])) {
			$image = $entry['pictures']['original'];
		} else {
			$image = $app_data['config']['public']['share_thumbnail_url']['value'];
		}

		$response = '<html>'
		 			.'<head>'
		 			.'<title>'.$title.'</title>'
				    .'<meta property="og:title" content="'.$title.'" />'
				    .'<meta property="og:type" content="article" />'
				    .'<meta property="og:url" content="http://'.$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"].'" />'
				    .'<meta property="og:image" content="'.$image.'" />'
				    .'<meta property="og:description" content="'.$description.'" />'
					.'</head>'
					.'<body>'
					.'<script>window.location.href="'.$app_data['config']['public']['app_fb_page_url']['value'].'?app_data={\"share_id\":'.$data['id'].'}";</script>'
					.'</body>'
					.'</html>';
		

		$res->write($response);
	}

}

?>