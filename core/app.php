<?php

/*
KriekApps API App Module
0.1b
*/

class App extends Api{

	function getAllApps(){
		global $params;
		if($params['user_id'] == 1) {
			$params['return_action']['sql'] = "SELECT id,name,config FROM apps ORDER BY name";
		}
	}

	function isAppInProduction(){
		global $app_data;
		
		if($app_data['config']['public']['production']['value'] == true) {
			$this->return_error("This feature is not allowed in production mode");
		}
	}

	function AppsFormat(){
		global $params;
		$temp = $params['return_data'];
		//print_r(is_array($params['return_data'][0])); die();
		$sql = "SELECT app_id,type,(SELECT name_short FROM activites WHERE id=a.type) AS name,COUNT(id) AS q FROM activity AS a GROUP BY app_id,type ORDER BY app_id";
		
		if(!is_array($params['return_data'][0])) {
			$temp = array($temp);
			$sql = "SELECT app_id,type,(SELECT name_short FROM activites WHERE id=a.type) AS name,COUNT(id) AS q FROM activity AS a WHERE app_id=:app_id GROUP BY app_id,type ORDER BY app_id";
		}

		$activity = $this->DB->runSQL($sql,null,'collection');
		//print_r($activity); die();

		$temp_id = 0;
		$rows = array();
		for ($i=0; $i < count($activity); $i++) { 
			if($temp_id != $activity[$i]['app_id']) {
				$temp_id = $activity[$i]['app_id'];
				$rows[$activity[$i]['app_id']] = array();
			}

			$rows[$activity[$i]['app_id']][$activity[$i]['type']] = array(
				"name"=>$activity[$i]['name'],
				"q"=>$activity[$i]['q']
			);
		}

		//print_r($rows); die();



		for ($i=0; $i < count($temp); $i++) { 
			$temp[$i]['config'] = json_decode($temp[$i]['config'],TRUE);
			$public = $temp[$i]['config']['public'];
			$temp[$i]['config'] = array(
					"app_fb_url"=>$public['app_fb_url'],
					"active_from"=>$public['active_from'],
					"active_until"=>$public['active_until'],
					"terms_and_conditions_url"=>$public['terms_and_conditions_url'],
					"basecamp_url"=>$public['basecamp_url'],
					"production"=>$public['production'],
					"archive"=>$public['archive']
				);
			$temp[$i]['activity'] = $rows[$temp[$i]['id']];
		}

		$params['return_data'] = $temp;
	}
	//new

	function getAppData($app_id){

			// $sql = "SELECT * FROM apps WHERE id=:app_id";
			// $data = $this->DB->runSQL($sql,array("app_id" => $params['app_id']),'model');
			// if($data){
			// 	$data['config'] = json_decode($data['config'],true);
			// 	$data['lang'] = json_decode($data['lang'],true);
			// 	$data['config']['public']['app_id'] = $app_id;
			// 	return $data;
					
			// } else {
			// 	$this->return_error("Invalid App ID");
			// }

			$sql = "SELECT * FROM apps WHERE id=:id";
			try {
				$db = $this->getConnection();
				$stmt = $db->prepare($sql);  
				$stmt->bindParam("id", $app_id);
				$stmt->execute();
				$data = $stmt->fetch(PDO::FETCH_ASSOC);

				if($data){
					$data['config'] = json_decode($data['config'],true);
					$data['lang'] = json_decode($data['lang'],true);
					$data['briefcase'] = json_decode($data['briefcase'],true);
					$data['config']['public']['app_id'] = $app_id;
					return $data;
					
				} else {
					$this->return_error("Invalid App ID");
				}

				$db = null;
	 
			} catch(PDOException $e) {
				$this->return_error($e->getMessage());
			}
	}

}

?>