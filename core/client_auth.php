<?php
/* KriekApps API ClientAuth Module */

//require_once 'api.php';

class Client_Auth extends Api{

	function validateCredentials($data){
		$sql = "SELECT id,name FROM clients WHERE username=:username AND pass=:pass";
		try {
			$db = $this->getConnection();
			$stmt = $db->prepare($sql);
			$stmt->bindParam("username", $data['username']);
			$stmt->bindParam("pass", $data['password']);
			$stmt->execute();
			$client = $stmt->fetch(PDO::FETCH_ASSOC);
			$db = null;

			if($client){
				return $client;
			} else {
				return false;
			}

		} catch(PDOException $e) {
			return false;
		}
	}

	function getAdminToken(){
		$params = $this->slim->request()->getBody();
		$expires = strtotime('+1 day', time());

		if($client = $this->validateCredentials($params)){
				$admin_token_data = array(
					"id" => $client['id'],
					"name" => $client['name'],
					"expire" => $expires
				);

				$admin_token_data['admin_token'] = $this->encryptData($admin_token_data);

				$this->return_json(array("status" => "success", "data" => $admin_token_data));

		} else {
			$this->return_error("Invalid username or password");
		}
	}

	function validateAdminToken($admin_token){
		$admin_token_data = $this->decryptData($admin_token);

		if(isset($admin_token_data['expire'])){

			if($admin_token_data['expire'] > time()){

				if(isset($_SESSION['app_id']) && $admin_token_data['id'] != 1){

					$sql = "SELECT id FROM apps WHERE client_id=:client_id AND id=:id";
					try {
						$db = $this->getConnection();
						$stmt = $db->prepare($sql);
						$stmt->bindParam("client_id", $admin_token_data['id']);
						$stmt->bindParam("id", $_SESSION['app_id']);
						$stmt->execute();
						$app = $stmt->fetch(PDO::FETCH_ASSOC);
						$db = null;

						if(!$app){
							return false;
						}

					} catch(PDOException $e) {
						$this->return_error($e->getMessage());
					}
				}

				if($admin_token_data['id'] == 1){
					$_SESSION['role'] == "superuser";
				}

			} else {
				$this->return_error("Error validating admin token: Session has expired at ".date('Y-m-d H:i:s',$admin_token_data['expire']));
			}

			return $admin_token_data;
		} else {
			return false;
		}
	}

}

?>
