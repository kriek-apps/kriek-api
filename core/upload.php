<?php
/* KriekApps API File Upload Module */

//require_once 'api.php';
require_once 'amazon.php';
require_once 'mimetypes.php';

class Upload extends Api{

	function __construct(){
		parent::__construct();
		global $app_data;
		$this->appdata = $app_data;
		//$this->appdata = App::getAppData($_SESSION['app_id']);
		$this->checkFolder();
	}

	/* Check if the app already has a folder otherwise it creates one */
	function checkFolder(){
		if(!file_exists($this->config['upload_folder'].$_SESSION['app_id'])){
			mkdir($this->config['upload_folder'].$_SESSION['app_id']);
			mkdir($this->config['upload_folder'].$_SESSION['app_id']."/app_assets/");
			mkdir($this->config['upload_folder'].$_SESSION['app_id']."/user_assets/");
		}
	}

	function fixImage($uploadedImage){
		$image = imagecreatefromstring(file_get_contents($uploadedImage));
		$exif = exif_read_data($uploadedImage);
		if(!empty($exif['Orientation'])) {
		    switch($exif['Orientation']) {
		        case 8:
		            $image = imagerotate($image,90,0);
		            break;
		        case 3:
		            $image = imagerotate($image,180,0);
		            break;
		        case 6:
		            $image = imagerotate($image,-90,0);
		            break;
		    }
		}

		return $image;
		// $image now contains a resource with the image oriented correctly
	}

	function saveFile(){
		// $verifyToken = md5('vivalakriek' . $_POST['timestamp']);
		// if (!empty($_FILES) && $_POST['token'] == $verifyToken) {
		if (!empty($_FILES)) {
			/* Get users role and set $role to folder */
			if($_SESSION['role'] == "admin"){
				$role = "/app/";
				$md5NewFile = "";
			} elseif ($_SESSION['role'] == "user") {
				$role = "/user/";
				$md5NewFile = $_SESSION['user']['id']."_";
			} else {
				$this->return_error("You don't have permission to upload files");
			}

			/* PHP temp filename */
			$tempFile = $_FILES['Filedata']['tmp_name'];

			/* Get file parts (extensions, name) */
			$fileParts = pathinfo($_FILES['Filedata']['name']);

			/* Set random filename */
			$md5NewFile .= md5($_FILES['Filedata']['name'].time().rand(1,9999)).'.'.$fileParts['extension'];
			
			/* Set target Path */
			//$targetPath = $this->config['upload_folder'].$_SESSION['app_id'].$role;
			
			/* Server side absolute file path */
			//$targetFile = rtrim($targetPath,'/') . '/' . $md5NewFile;
			
			// Validate the file type
			$fileTypes = explode(',',$this->appdata['config']['public']['modules']['ugc']['allowed_fietypes']);
			
			if (in_array($fileParts['extension'],$fileTypes) || $_SESSION['role'] == "admin") {
				/* Copy uploaded file */
				if(strtolower($fileParts['extension']) == "jpg") {
					$imgFix = $this->fixImage($tempFile);
					imagejpeg($imgFix, $tempFile, 100);
					imagedestroy($imgFix);
				}

				$mime = new Mimetypes();
				$this->Amazon = new Amazon();
				$result = $this->Amazon->uploadBucket(
					array(
						'Key' => "assets" . $role . $md5NewFile,
    					'SourceFile' => $tempFile,
    					'ContentType' => $mime->findType($fileParts['extension'])
    			));

				/* return data */
				$this->return_json(array("file_url" => $result));

			} else {
				$this->return_error('Invalid file type');
			}

		}
	}

}