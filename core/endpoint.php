<?php
/*
KriekApps API UGC Module
0.1b
*/

//require_once 'api.php';

class Endpoint extends Api{

	function getRoute($url){
		$ends = $this->memcache->get('endpoints_new');

		if(!isset($ends) || $ends == "") {
			$ends_data = $this->DB->runSQL("SELECT * FROM endpoints ORDER BY collection",null,'collection');
			foreach ($ends_data as $key => $value) {
				$value['GET'] = json_decode($value['GET'],TRUE);
				$value['POST'] = json_decode($value['POST'],TRUE);
				$value['PUT'] = json_decode($value['PUT'],TRUE);
				$value['DELETE'] = json_decode($value['DELETE'],TRUE);
				$value['collection'] = explode("/", $value['collection']);

				$ends[]=$value;
			}
			$this->setMemcacheValue('endpoints_new',$ends);
		}

		foreach ($ends as $value) {
			//$endpoints = explode("/", $value['collection']);
			$endpoints = $value['collection'];
			if(count($endpoints) == count($url)) {
					$temp = true;
					$params = array();
					foreach ($endpoints as $urlpoz => $urlvalue) {
						if(substr($urlvalue, 0, 1) != ':') {
							if($endpoints[$urlpoz] != $url[$urlpoz]) {
								$temp = false;
							}
						} else {
							$params[substr($urlvalue, 1)] = $url[$urlpoz];
						}
					}

					if($temp) {
						$collection = $value[$_SERVER['REQUEST_METHOD']]; //json_decode($value[$_SERVER['REQUEST_METHOD']],TRUE);
						$params['return_settings'] = $collection['settings'];
						$params['return_action'] = $collection['roles'][$_SESSION['role']];

						if($params['return_action']['sql'] != null) {
							$this->collectParams($params);
							return true;
						}
						
					}
				}
		}
		return false;
	}

	function collectParams($url_params){
		/* Load all variables into $params global variable
		   All POST GET HEAD BODY COOKIE variables will be collected
		   Watch for variable name assigment! (AVOID USING THE RESERVED NAMES!!)
		   RESERVED variables NAMES:
		   -session_     will be deleted!
		   -return_      will be deleted!
		   -cookie_
		   -head_
		   -user_        will be deleted!
		   -status_
		   -roles        will be deleted!
		   -data_
		   -return_      will be deleted!
		*/
		global $params;

		$params = $this->slim->request()->getBody();  //get body variables if json encoded
		$cookies = $this->slim->request()->cookies(); //get cookie variables
		$headers = $this->slim->request()->headers(); //get header variables

		if($_GET['_method']){
			$params = json_decode($params,TRUE);
		}

		if(is_array ($params)) {
			$params = array_merge($params, $this->slim->request()->get()); //if $params were json encoded, than merge witg GET variables if present
		} else {
			$params = $this->slim->request()->params(); //if $params wwwform url encoded read all GET, POST variables
		}

		//unset($params['debug']);			//uncomment this line if you want to disable debug
		if(!$this->config['developer']) {	//remove in production >>>>>>
			unset($params['debug']);		//if in config/config.php developer is false debug will be disabled
		}	//<<<<<<<remove in production

		//SECURITY CHECK
		foreach ($params as $key => $value) {
			$pos = strrpos(strtolower($key), "return_");	//any parameter containing session_ will be destroyed for security reasons.
			if ($pos === false) {} else { 
				unset($params[$key]); 
			}
			$pos = strrpos(strtolower($key), "user_");	//any parameter containing user_ will be destroyed for security reasons.
			if ($pos === false) {} else { 
				unset($params[$key]); 
			}
			$pos = strrpos(strtolower($key), "session_");	//any parameter containing roles will be destroyed for security reasons.
			if ($pos === false) {} else { 
				unset($params[$key]); 
			}
		}
		//SECURITY CHECK
		if(isset($params['app_id'])) {
			$url_params['app_id'] = $params['app_id'];
		}

		$params = array_merge ($params, $url_params);

		if($cookies != null) {
			foreach ($cookies as $key => $value) {
			 	$params['cookie_'.$key]=$value;			//collect all cookie parameters
			}
		}

		// if($headers != null) {
		// 	foreach ($headers as $key => $value) {
		// 	 	$params['head_'.$key]=$value;			//collect all head parameters
		// 	}
		// }

		if($_SESSION != null) {
			foreach ($_SESSION as $key => $value) {
				if($key == "user") {
					foreach ($value as $user_key => $user_value) {
						$params['user_'.$user_key]=$user_value;	//put session_user parameters to user_
					}
				} 
				$params['session_'.$key]=$value; // collect all session variables
			}
		}

		if(isset($params['debug'])) {						//remove in production >>>>>>												
			echo "Initial parameters:\n";					//debug
			print_r($params);								
		}													//<<<<<<<remove in production

		$this->processCollection(); //call collection processor
	}

	function processCollection(){
		global $params;

		if(!isset($params['app_id'])) {
			$params['app_id'] =  $_SESSION['app_id'];
		}

		if(!isset($params['group_id'])) {
			$params['group_id'] = null;
		}

		if(isset($params['return_action']['before'])) {
			$this->runConditions($params['return_action']['before']); //run any conditions before
		}

		$sql = $params['return_action']['sql'];

		if(trim($sql) == "XXX") {
			$this->slim->stop();
		}

		$params['return_data'] = $this->DB->runSQL($sql,null,$params['return_settings']['type']);

		if(isset($params['return_action']['after'])) {
			$this->runConditions($params['return_action']['after']); //run any conditions after
		}

		$this->return_json($params['return_data']);
	}

	function runConditions($conditions){
		global $params;

		foreach ($conditions as $key => $value) {
			$class = explode('::', key($value));
			if (!class_exists($class[0])) {
				$temp = "core/".strtolower($class[0]);
				if($class[0] == "Custom") {
					$temp = "core/custom/".$_SESSION['app_id'];
				}
				try {
					require_once $temp.'.php';
				} catch(exeption $e) {
					$this->return_error($e->getMessage());
				}
			}
			$app = new $class[0]();
			foreach ($value[key($value)] as &$param) {
				if(!is_array($param)) {
					if(substr($param,0,1) == ":") {
						$param = $this->extractParam(substr($param,1),$params); //$params[substr($param,1)];
					}
				}
			}
			call_user_func(array($app, $class[1]),$value[key($value)]);
			unset($app);
		}
	}

	function extractParam($param,$params){
		$temp = explode('=>', $param);
		$returnParam = $params;

		foreach ($temp as $key => $value) {
			if(isset($returnParam[$value])) {
				$returnParam = $returnParam[$value];
			} else {
				$this->return_error($param." is missing parameter ".$value);
			}
		}

		return $returnParam;
	}

}