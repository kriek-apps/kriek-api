<?php
/* KriekApps API User Module */

class Mimetypes extends Api{

	function findType($obj){
		$string = file_get_contents("core/mimetypes.json");
		$json = json_decode($string, true);

		$obj=str_replace(".","",$obj);

		foreach ($json as $key => $value) {
			if($obj == $key){
				return $value;
			}
		}

		return 'text/html';
	}

}