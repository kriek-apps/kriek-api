<?php

/* KriekApps API Invite Module */

class Invite extends Api{
	function runDelete(){
		global $params;
		$this->deleteInvite($params['request_id'], $params['user_id']);
	}

	function deleteInvite($request_id,$user_id){
		global $app_data;
		$this->app_data = $app_data;
		//$this->app_data = App::getAppData($_SESSION['app_id']);

		/*IF testcase dont try to delete*/
		if($request_id == 123456789 && $this->config['developer']){
			return true;
		} else {
			$fb = new FB();
			try{
				return $fb->fb->api('/'.$request_id.'_'.$user_id, 'DELETE', array("access_token" => $this->app_data['config']['private']['app_token']['value']));
			} catch(Exception $e){
				$this->return_error($e->getMessage().'-'.$request_id.'_'.$user_id);
			}
		}
	}

	function confirmInvite(){
		global $params;
		$this->deleteInvite($params['request_id'], $params['user_id']);
		$sql = "UPDATE invites SET confirmed = true WHERE request_id=:request_id AND invitee_id=:user_id AND app_id=:app_id";
		$data= $this->DB->runSQL($sql);

		if($data['affected_rows'] > 0){
			$this->activityFeed(array(6,$params['request_id']));
			$this->return_json(array( "status" => "success", "message" => "Invite Confirmed"));
		} else {
			$this->return_error("We couldnt confirm this invite ");
		}
	}

	function addInvite(){
		global $params;

		// Get invite request id
		$request_id = $params['request'];

		//Get users
		$invited_users = $params['to'];

		$added = array();
		$rejected = array();

		foreach($invited_users as $invitee_id){
			$sql = "INSERT INTO invites (app_id, request_id, inviter_id, invitee_id,group_id) VALUES (:app_id, :request_id, :inviter_id, :invitee_id,:group_id)";
			try {
				$db = $this->getConnection();
				$stmt = $db->prepare($sql);  
				$stmt->bindParam("app_id", $_SESSION['app_id']);
				$stmt->bindParam("group_id", $params['group_id']);
				$stmt->bindParam("request_id", $request_id);
				$stmt->bindParam("inviter_id", $_SESSION['user']['id']);
				$stmt->bindParam("invitee_id", $invitee_id);
				$stmt->execute();
				$db = null;
				array_push($added,$invitee_id);

			} catch(PDOException $e) {
				array_push($rejected,$invitee_id);
				$this->deleteInvite($request_id,$invitee_id);
			}
		}

		$this->return_json(array("status" => "success", "message" => "Invites stored", "added" => $added, "rejected" => $rejected));
	}

	function getRequest(){
		$sql = "SELECT DISTINCT(i.inviter_id), u.name, i.request_id FROM invites AS i JOIN users AS u ON u.id=i.inviter_id WHERE (SELECT FIND_IN_SET (i.request_id, :ids)) != 0 AND u.app_id = :app_id";
		$this->return_json($this->DB->runSQL($sql,null,'collection'));
	}

}

?>