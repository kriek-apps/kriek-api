<?php
/* KriekApps API Memcache Module */

class MemcacheApi extends Api{

	function checkTable($attr) {
		$data = $this->getTable($attr);

		if($data != "") {
			$this->return_memcache($data);
		}

		global $params;
		$params['memcache'] = array("key"=>$attr[0],"id"=>$attr[1]);
	}

	function getTable($attr){
		return $this->memcache->get($attr[0].$attr[1]);
	}

	function syncTable($attr, $json){
		$this->setMemcacheValue($attr['key'].$attr['id'],gzencode($json,9));
	}

	function deleteMemcache($attr){
		$this->memcache->delete($attr[0].$attr[1]);
	}

	function flushMemcache(){
		$this->memcache->flush();
		/* return data */
		$this->return_json(array("message" => "Memcache emptied"));
	}

	function listMemcache(){
		global $params;
		if($params['user_id'] != 1) {
			return;
		}
		$mem = $this->memcache->get('@keys');
		$data = $this->memcache->get($mem);
		foreach ($data as $key => $value) {
			if(!is_array($value)) {
				if (strlen ($value) < 18 || strcmp(substr($value,0,2),"\x1f\x8b")) {
			       
			    } else {
			    	$data[$key] = array("gzip" => json_decode(gzdecode($value),TRUE));
			    }
			}
		}

		$this->return_json($data);
	}

}