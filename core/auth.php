<?php
/* KriekApps API Auth Module */

class Auth extends Api{
	
	function __construct($app_id = null){
		global $params;
		parent::__construct();


		/* User */
		if($access_token = $this->slim->request()->params('access_token') ){

			session_name('kriekapp_'.$app_id);
			session_start();

			$_SESSION['app_id'] = $app_id;
			$_SESSION['role'] = 'user';


			if(!isset($_SESSION['user']) OR $access_token != $_SESSION['access_token']){

				$_SESSION['access_token'] = $access_token;

				/* Init Facebook Object*/
				$fb = new FB();

				/* Get User Data */
				$_SESSION['user'] = $fb->getCurrentFBUser();

				/* Get current user group */
				$sql = "SELECT `group` FROM users WHERE app_id = :app_id AND id = :user_id";
				$insert = array(
					"app_id" => $_SESSION['app_id'],
					"user_id" => $_SESSION['user']['id']
				);
				$data = $this->DB->runSQL($sql,$insert,"collection");
				$_SESSION['user']['group'] = $data[0]['group'];

				/* Register if not yet registered */
				User::registerUser();

			} else {
				$_SESSION['access_token'] = $access_token;
			}

			
		/* Admin */
		} else if($admin_token = $this->slim->request()->params('admin_token')){

			session_name('kriekadmin');
			session_set_cookie_params(0,"/admin/");
			session_start();
			$_SESSION['app_id'] = $app_id;
			$_SESSION['admin_token'] = $admin_token;

			if($user = Client_Auth::validateAdminToken($admin_token)){
				
				/* Setup Session for Admin */
				$_SESSION['role'] = 'admin';
				$_SESSION['user'] = $user;

			} else {
				$this->return_error('Invalid token');
			}

		/* Unknown */
		} else {
			session_name('kriekapp_'.$app_id);
			session_start();
			$_SESSION['app_id'] = $app_id;
			$_SESSION['role'] = 'unknown';
		}

	}
}

?>