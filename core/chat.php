<?php
/* KriekApps API User Module */

class Chat extends Api{

	function getUpdate($name){
		setcookie($name[0], time());
	}

	function postQuestion(){
		global $params;
		$sql = "SELECT data,
				(SELECT COUNT(id) FROM chat_questions WHERE topic=:topic_id AND user_id=:user_id) AS q_count, 
				(SELECT enabled FROM users WHERE app_id=:app_id AND id=:user_id) AS enabled 
				FROM chat_topics
				WHERE id=:topic_id 
				AND app_id=:app_id";
		$insert = array(
			":app_id"=>$params['app_id'],
			":topic_id"=>$params['topic_id'],
			":user_id"=>$params['user_id']
		);

		try {

			$db = $this->getConnection();
			$stmt = $db->prepare($sql);  
			$stmt->execute($insert);
			$data = $stmt->fetch(PDO::FETCH_ASSOC);						
			$db = null;

			$data['data'] = json_decode($data['data'],TRUE);

			if($data['data']['open'] == "false") {
				$this->return_error(array(id=>400,txt=>"This topic is closed"));
			}
			if(is_array($params['question'])) {
				$this->return_error(array(id=>401,txt=>"Not a string"));
			}
			if(!isset($params['question'])) {
				$this->return_error(array(id=>402,txt=>"Question field is empty"));
			}
			if(!isset($data['enabled'])) {
				$this->return_error(array(id=>403,txt=>"User not registered for this app"));
			}
			if(!$data['enabled']) {
				$this->return_error(array(id=>404,txt=>"User has been banned for this app"));
			}

			if(isset($data['data']['maxq'])) {
				if($data['q_count']>=$data['data']['maxq']) {
					$this->return_error(array(id=>405,txt=>"User has no question left"));
				}
			}

			$json = array();
			$json['uid'] = $params['user_id'];
			$json['name'] = $params['user_name'];
			$json['txt'] = htmlspecialchars ($params['question']);
			$json['date'] = date("Y-m-d H:i:s");
			if (isset($params['extra'])) {
				$json['extra'] = $params['extra'];
			}
			$json['answers'] = array();

			$params['visible'] = true;
			if(isset($data['data']['premoderation'])) {
				$params['visible'] = false;
			}
			$params['data'] = json_encode($json);

		} catch(PDOException $e) {
			$this->return_error($e->getMessage());
		}
	}

	function notif_admin_after_question(){
		global $params;

		$this->emailNotifAdmin($params['question'],$params['return_id']);
	}

	function putAnswer(){
		global $params;
		$sql = "SELECT data,
				(SELECT enabled FROM users WHERE app_id=:app_id AND id=:user_id) AS enabled,
				(SELECT data FROM chat_questions WHERE id=:id AND user_id=:user_id AND visible=1) AS question
				FROM chat_topics
				WHERE id=(SELECT topic FROM chat_questions WHERE id=:id)
				AND app_id=:app_id";
		$insert = array(
			":app_id"=>$params['app_id'],
			":id"=>$params['question_id'],
			":user_id"=>$params['user_id']
		);
		try {

			$db = $this->getConnection();
			$stmt = $db->prepare($sql);  
			$stmt->execute($insert);
			$data = $stmt->fetch(PDO::FETCH_ASSOC);						
			$db = null;

			$data['data'] = json_decode($data['data'],TRUE);
			$data['question'] = json_decode($data['question'],TRUE);

			if($data['data']['open'] == "false") {
				$this->return_error("This topic is closed");
			}
			if(is_array($params['answer'])) {
				$this->return_error("Not a string");
			}
			if(!isset($params['answer'])) {
				$this->return_error("Empty answer");
			}
			if(!isset($data['question'])) {
				$this->return_error("Unknown question");
			}
			if(!isset($data['enabled'])) {
				$this->return_error("User not registered for this app");
			}
			if(!$data['enabled']) {
				$this->return_error("User has been banned for this app");
			}

			$lastpost = end($data['question']['answers']);

			if($lastpost['uid'] == $params['user_id'] || count($data['question']['answers']) == 0) {
				$this->return_error("Only one answer is allowed");
			}

			$json = array();
			$json['uid'] = $params['user_id'];
			$json['name'] = $params['user_name'];
			$json['txt'] = htmlspecialchars ($params['answer']);
			if (isset($params['extra'])) {
				$json['extra'] = $params['extra'];
			}
			$json['date'] = date("Y-m-d H:i:s");

			array_push ($data['question']['answers'],$json);

			$params['data'] = json_encode($data['question']);

			$this->emailNotifAdmin($json['txt'],$params['question_id']);

		} catch(PDOException $e) {
			$this->return_error($e->getMessage());
		}

	}

	function putAdminAnswer(){
		global $params;
		$sql = "SELECT user_id,data FROM chat_questions WHERE id=:id";
		$insert = array(
			":id"=>$params['question_id']
		);
		try {

			$db = $this->getConnection();
			$stmt = $db->prepare($sql);  
			$stmt->execute($insert);
			$data = $stmt->fetch(PDO::FETCH_ASSOC);						
			$db = null;

			$data['data'] = json_decode($data['data'],TRUE);

			$params['answer']['date'] = date("Y-m-d H:i:s");

			array_push ($data['data']['answers'],$params['answer']);

			$params['data'] = json_encode($data['data']);

			$this->userFacebookNotif($data);

		} catch(PDOException $e) {
			$this->return_error($e->getMessage());
		}

	}

	function deleteAdminAnswer(){
		global $params;
		$sql = "SELECT data FROM chat_questions WHERE id=:id";
		$insert = array(
			":id"=>$params['question_id']
		);
		try {

			$db = $this->getConnection();
			$stmt = $db->prepare($sql);  
			$stmt->execute($insert);
			$data = $stmt->fetch(PDO::FETCH_ASSOC);						
			$db = null;

			$data['data'] = json_decode($data['data'],TRUE);

			array_splice($data['data']['answers'],$params['answer_id'],1);
			$params['data'] = json_encode($data['data']);

		} catch(PDOException $e) {
			$this->return_error($e->getMessage());
		}

	}

	function putAdminUser(){
		global $params;
		$sql = "UPDATE users SET enabled=:enabled WHERE id=:user_id AND app_id=:app_id";
		$insert = array(
			":user_id"=>$params['user_id'],
			":app_id"=>$params['app_id'],
			":enabled"=>$params['visible']
		);
		try {

			$db = $this->getConnection();
			$stmt = $db->prepare($sql);  
			$stmt->execute($insert);

		} catch(PDOException $e) {
			$this->return_error($e->getMessage());
		}

	}

	function addSQL(){
		global $params;

		if(isset($params['last_updated'])) {
			$params['return_action']['sql'] .= " AND date>=:last_updated";
		} 

		if(isset($params['limit']) && isset($params['offset'])) {
			if(is_numeric($params['limit']) && is_numeric($params['offset'])) {
				$params['return_action']['sql'] .= " ORDER BY id DESC LIMIT ".abs((int)$params['limit']).",".abs((int)$params['offset']);
			}
		} elseif(isset($params['limit'])) {
			if(is_numeric($params['limit'])) {
				$params['return_action']['sql'] .= " LIMIT ".abs((int)$params['limit']);
			}
		}

	}

	function emailNotifAdmin($txt,$id){
		require_once 'email.php';

		global $params;
		global $app_data;
		
		//$app_data = App::getAppData($params['app_id']);
		$admins = $app_data['config']['public']['modules']['discussion']['admins'];
		$email['from'] = $app_data['lang']['admin_email_notif_from'];
		if($admins){
			foreach($admins as $admin){
				$email['to'][] = $admin['email'];
			}
		}
		$email['subject'] = $app_data['lang']['admin_email_notif_subject'];
		$email['html'] = "Question: <br />".$txt."<br /><br />";
		$email['html'] .= str_replace("::id", $id, $app_data['lang']['admin_email_notif_html']);

		$mailer = new Email();
		$mailer->send($email,false);
	}

	function userFacebookNotif($question_data){
		require_once 'facebook.php';
		global $params;
		global $app_data;

		//$app_data = App::getAppData($params['app_id']);
		$app_token = $app_data['config']['private']['app_token']['value'];
		
		$template = $app_data['lang']['user_fb_notif_text'];
		$href = "index.php?question_id=".$params['question_id'];
		$user_id = $question_data['user_id'];

		$fb = new FB();
		$return = $fb->fbapi('/'.$user_id.'/notifications','POST',array(
			"access_token" => $app_token,
			"template" => $template,
			"href" => $href
		));

	}

}