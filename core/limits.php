<?php
/* KriekApps API Limiter Module */

class Limits extends Api{

	function checkTimeRange($parameters){
		global $app_data;
		$parameters = $parameters[0];

		$module = $app_data['config']['public']['modules'][$parameters['module']];

/*		MIELOTT LIVE LENNE MEGKELL MEGKELL NEZNI MINDEN APPOT
		if(!$module['active']) {
			$this->return_error($parameters['module'].' module is turned off.');
		}*/

		$from = $module[$parameters["from"]];
		$till = $module[$parameters["till"]];

		//print_r($module);die();

		if(!isset($from)  || !isset($till)) {
			return;
		}

		if($from == ''  || $till == '') {
			return;
		}

		$now = time();
		if(strtotime($from) < $now && $now < strtotime($till)) {
			return;
		} else {
		 	$this->return_error($parameters['module'].' module is inactive.');
	 	}
	}

	function checkLimits(){
		global $app_data;
		global $params;

		$limits = $app_data['config']['public']['modules']['limits'];

		$a = (time() - strtotime($limits['day_start'])) / 86400; //days the campaign has been going
		$b = ceil($a / $limits['days_per_cycle']); //current cycle

		// cycle date we query from, current round - 1 end date
		$cycle_from_date = date('Y-m-d H:i:s', strtotime($limits['day_start']) + 86400 * $limits['days_per_cycle'] * ($b-1) );

		// query for cycle
		$cycle_check_sql = "SELECT count(*) as played FROM limits WHERE `date` >= :from_date AND app_id = :app_id AND user_id = :user_id ";
		$cycle_check_params = array(
			"from_date" => $cycle_from_date,
			"app_id" => $params['app_id'],
			"user_id" => $params['user_id']
		);

		// get number of playes in current cycle
		$cycle_data = $this->DB->runSQL($cycle_check_sql, $cycle_check_params,"model");

		// Check if allowed
		if($cycle_data['played'] >= $limits['allowed_per_cycle']){
			$this->return_error("You cannot play more in this cycle");
		}

		// daily date query from
		$daily_from_date = date('Y-m-d H:i:s', strtotime($limits['day_start']) + floor($a) * 86400 );

		$daily_check_sql = "SELECT count(*) as played FROM limits WHERE `date` >= :from_date AND app_id = :app_id AND user_id = :user_id ";
		$daily_check_params = array(
			"from_date" => $daily_from_date,
			"app_id" => $params['app_id'],
			"user_id" => $params['user_id']
		);

		// get number of playes in current daily
		$daily_data = $this->DB->runSQL($daily_check_sql, $daily_check_params,"model");
		$params['tries_left'] = $limits['allowed_daily']-$daily_data['played'];

		// Check if allowed
		if($daily_data['played'] >= $limits['allowed_daily']){
			$this->return_error("You cannot play more today");
		}

	}

}