<?php

Class Insights extends Api{

	function userFlow13(){
		$sql = "SELECT SUM(id) AS q FROM (
				SELECT COUNT(c.uid) AS id FROM (
				SELECT * FROM (SELECT DISTINCT(user_id) AS uid FROM activity WHERE activity.type=113 AND user_id!=0 AND app_id=:app_id AND (activity.group=:group OR 'all'=:group)) AS a
				WHERE uid NOT IN 
				(SELECT * FROM (SELECT DISTINCT(user_id) AS uid FROM activity WHERE activity.type=13 AND user_id!=0 AND app_id=:app_id AND (activity.group=:group OR 'all'=:group)) AS b)
				) AS c
				UNION ALL
				SELECT COUNT(c.uid) AS id FROM (
				SELECT * FROM (SELECT DISTINCT(session) AS uid FROM activity WHERE activity.type=113 AND user_id=0 AND app_id=:app_id AND (activity.group=:group OR 'all'=:group)) AS a
				WHERE uid NOT IN 
				(SELECT * FROM (SELECT DISTINCT(session) AS uid FROM activity WHERE activity.type=13 AND user_id=0 AND app_id=:app_id AND (activity.group=:group OR 'all'=:group)) AS b)
				) AS c
				UNION ALL
				SELECT COUNT(a.uid) FROM (SELECT DISTINCT(user_id) AS uid FROM activity WHERE type = 13 AND user_id!=0 AND app_id=:app_id AND (activity.group=:group OR 'all'=:group)) AS a
				UNION ALL
				SELECT COUNT(a.uid) FROM (SELECT DISTINCT(session) AS uid FROM activity WHERE type = 13 AND user_id=0 AND app_id=:app_id AND (activity.group=:group OR 'all'=:group)) AS a
				) AS d";
		$result = $this->DB->runSQL($sql,null,'model');
		return $result['q'];
	}


	function userFlow14(){
		$sql = "SELECT SUM(id) AS q FROM (
				SELECT COUNT(c.uid) AS id FROM (
				SELECT * FROM (SELECT DISTINCT(user_id) AS uid FROM activity WHERE activity.type=13 AND user_id!=0 AND app_id=:app_id AND (activity.group=:group OR 'all'=:group)) AS a
				WHERE uid NOT IN 
				(SELECT * FROM (SELECT DISTINCT(user_id) AS uid FROM activity WHERE activity.type=113 AND user_id!=0 AND app_id=:app_id AND (activity.group=:group OR 'all'=:group)) AS b)
				) AS c
				UNION ALL
				SELECT COUNT(c.uid) AS id FROM (
				SELECT * FROM (SELECT DISTINCT(session) AS uid FROM activity WHERE activity.type=13 AND user_id=0 AND app_id=:app_id AND (activity.group=:group OR 'all'=:group)) AS a
				WHERE uid NOT IN 
				(SELECT * FROM (SELECT DISTINCT(session) AS uid FROM activity WHERE activity.type=113 AND user_id=0 AND app_id=:app_id AND (activity.group=:group OR 'all'=:group)) AS b)
				) AS c
				UNION ALL
				SELECT COUNT(a.uid) AS id FROM ((SELECT DISTINCT(user_id) AS uid FROM activity WHERE type = 113 AND user_id!=0 AND app_id=:app_id AND (activity.group=:group OR 'all'=:group)) AS a INNER JOIN (SELECT DISTINCT(user_id) AS uid FROM activity WHERE type = 14 AND user_id!=0 AND app_id=:app_id AND (activity.group=:group OR 'all'=:group)) AS b ON a.uid=b.uid)
				UNION ALL
				SELECT COUNT(a.uid) AS id FROM ((SELECT DISTINCT(session) AS uid FROM activity WHERE type = 113 AND user_id=0 AND app_id=:app_id AND (activity.group=:group OR 'all'=:group)) AS a INNER JOIN (SELECT DISTINCT(session) AS uid FROM activity WHERE type = 14 AND user_id=0 AND app_id=:app_id AND (activity.group=:group OR 'all'=:group)) AS b ON a.uid=b.uid)
				) AS d";
		$result = $this->DB->runSQL($sql,null,'model');
		return $result['q'];
	}

	function userFlow(){
		global $app_data;
		global $params;

		if(!isset($params['group'])) {
			$params['group'] = 'all';
		}

		$userFlow = $app_data['config']['private']['userFlow']['value'];
		$userFlow = explode(',', $userFlow);

		$resultExtra = array();
		foreach ($userFlow as $key => $value) {
			if($value == "13") {
				$resultExtra[] = array('name' => 'Visits','type' => 13,'q' => $this->userFlow13() );
				unset($userFlow[$key]);
			} else if($value == "14") {
				$resultExtra[] = array('name' => 'Likes','type' => 14,'q' => $this->userFlow14() );
				unset($userFlow[$key]);
			} 
		}

		$userFlow = implode(",", $userFlow);
		$result = array();
		if($userFlow != "") {
			$sql = "SELECT name_chart AS name,type,COUNT(type) AS q FROM (SELECT DISTINCT(tb1.user_id) AS user,tb1.type,tb2.name_chart FROM activity AS tb1 LEFT JOIN activites AS tb2 ON tb1.type = tb2.id WHERE tb1.app_id=:app_id AND tb1.user_id!=0 AND (tb1.group=:group OR 'all'=:group)) AS a WHERE type IN (".$userFlow.") GROUP BY type";
			$data = $this->DB->runSQL($sql,null,'collection');

			$sql = "SELECT name_chart AS name,type,COUNT(type) AS q FROM (SELECT DISTINCT(tb1.session) AS user,tb1.type,tb2.name_chart FROM activity AS tb1 LEFT JOIN activites AS tb2 ON tb1.type = tb2.id WHERE tb1.app_id=:app_id AND tb1.user_id=0 AND (tb1.group=:group OR 'all'=:group)) AS a WHERE type IN (".$userFlow.") GROUP BY type";
			$data2 = $this->DB->runSQL($sql,null,'collection');

			foreach ($data as $key => &$value) {
				$type = $value['type'];
				foreach ($data2 as $key2 => $value2) {
					if($type==$value2['type']) {
						$value['q'] += $value2['q'];
						unset($data2[$key2]);
					}
				}
			}

			$result = array_merge($data, $data2);
		}

		//print_r($result);die();

		$this->return_json(array_merge($result, $resultExtra));
	}

	function activity(){
		$sql = "SELECT DATE(a.date) AS date,a.`type`,(SELECT `name_chart` FROM activites WHERE id = a.`type`) AS name,COUNT(a.app_id) AS result FROM activity AS a WHERE app_id=:app_id GROUP BY DATE(date),a.`type` ORDER BY date,a.`type`";
		$data = $this->DB->runSQL($sql,null,'collection');

		$header = array();
		$header["date"] = 0;
		$rows = array();
		$rows['activities'] = '';
		$rowsPointer = 0;
		$date = null;

		for ($i=0; $i < count($data); $i++) { 
			$value = $data[$i];

			$key = array_search($value['type'],$header);
			if($key === false) {
				$header[$value['name']] = $value['type'];
			}

			if($date != $value['date']) {
				if($date != null) {
					$date1=date_create($date);
					$date2=date_create($value['date']);
					$diff=date_diff($date1,$date2);

					for ($a=1; $a < $diff->days; $a++) { 
						date_add($date1, date_interval_create_from_date_string('1 days'));
						$rows[$date1->format('Y-m-d')] = array();
					}
				}

				$date = $value['date'];
				$rows[$date] = array();
			}
			$rows[$date][$value['type']] = $value['result'];
		}

		$head = array();
		foreach ($header as $key => $value) {
			if($value != 113) {
				$head[] = array("id"=>$value,"name"=>$key);
			}
			# code...
		}
		$rows['activities'] = $head;

		foreach ($rows as $key => &$value) {
			if(isset($value[113])) {
				if(!isset($value[13])) { $value[13] = 0; }
				$value[13] += $value[113];
				unset($value[113]);
			}
		}

		//print_r($diff);die();
		$this->return_json($rows);
	}

	function getChat($app_id){
			$sql = "SELECT * FROM chat_questions WHERE app_id=:app_id";
			try {
				$db = $this->getConnection();
				$stmt = $db->prepare($sql);
				$stmt->bindParam("app_id", $app_id); 
				$stmt->execute();
				$data = $stmt->fetchAll(PDO::FETCH_ASSOC);
				$db = null;

				return $data;
	 
			} catch(PDOException $e) {
				$this->return_error($e->getMessage());
			}
	}

	function insightsChat($app_id){
		$chat = $this->getChat($app_id[0]);
		$data = array();

		foreach ($chat as $key => &$value) {
			$value['data'] = json_decode($value['data'],TRUE);

			$temp = substr($value['data']['date'],0,10);

			if(isset($data[$temp])) {
				$data[$temp]['questions']++;
			} else {
				$data[$temp]['questions'] = 1;
			}

			foreach ($value['data']['answers'] as $key2 => $value2) {
				$temp = substr($value2['date'],0,10);

				if($value2['admin']) {
					if(isset($data[$temp])) {
						$data[$temp]['answers']++;
					} else {
						$data[$temp]['answers'] = 1;
					}
				} else {
					if(isset($data[$temp])) {
						$data[$temp]['questions']++;
					} else {
						$data[$temp]['questions'] = 1;
					}
				}
			}
		}

		foreach ($data as $key => $val){
			$formated = array(
				"date" => $key
			);
			$return[] = array_merge($formated,$val);
		}

		$this->return_json($return);
	}

}