<?php
/* KriekApps API DB Module */

/* Config */
require_once 'config/config.php';
require_once 'api.php';

class DB{

	function __construct(){
		global $config;

		$this->config = $config;
	}

	function collectParams($sql,$localParams){
		global $params;

		$this->api = new Api();
		$endpoint = new Endpoint();

		$returnParams = array();
		if($localParams == null) {
			$tempArray = $params;
		} else {
			$tempArray = $localParams + $params;
		}
		$strStart = 0;
		$sql = $sql." ";
		for ($i=0;$i<strlen($sql);$i++) {
			if($sql[$i] == ":") {
				$strStart = $i;
			} else if(
				($sql[$i] == " " && $strStart != 0) || 
				($sql[$i] == "," && $strStart != 0) || 
				($sql[$i] == ")" && $strStart != 0)) {
				$tempStr = substr($sql,$strStart+1,$i-$strStart-1);
				$strStart = 0;

				$value = $endpoint->extractParam($tempStr,$tempArray); //$tempArray[$tempStr];
				if(is_array($value)) {
					$value = json_encode($value);
				}
				$returnParams[str_replace("=>", "Y_Y",$tempStr)] = $value;
				//print_r($sql); die();

				// if(isset($tempArray[$tempStr])) {
				// 	$value = $endpoint->extractParam($tempStr,$tempArray); //$tempArray[$tempStr];
				// 	if(is_array($value)) {
				// 		$value = json_encode($value);
				// 	}
				// 	$returnParams[$tempStr] = $value;
				// } else {
				// 	$this->api->return_error("Parameter ".$tempStr." is missing.");
				// }
			}
		}

		//print_r($returnParams); die();

		return $returnParams;
	}

	function checkType($sql,$type){
		$sql = strtolower(trim($sql));
		if($type == "collection" || $type == "model"){
			if(substr($sql, 0, 6) != 'select') {
				$this->api->return_error("Only select is allowed in this query");
			}
		}
	}

	function runSQL($sql,$params=null,$type=null){
		$this->checkType($sql,$type);
		$params = $this->collectParams($sql,$params);

		$sql = str_replace("=>", "Y_Y",$sql);

		try {
			$db = $this->api->getConnection();
			//$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, FALSE);
			$stmt = $db->prepare($sql);
			$stmt->execute($params);

			if($type == "collection"){
				if(! $data = $stmt->fetchAll(PDO::FETCH_ASSOC)){
					$data = array();
				}
			} else if($type == "model") {			
				if(! $data = $stmt->fetch(PDO::FETCH_ASSOC) ){
					$data = new stdClass();
				}
			} else {
				$data['status'] = "success";
				$data['last_id'] = $db->lastInsertId();
				$data['affected_rows'] = $stmt->rowCount();
			}
			$db = null;

			return $data;
 
		} catch(PDOException $e) {
			$this->api->return_error($e->getMessage());
		}
	}

}