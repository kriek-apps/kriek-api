<?php
/* KriekApps API ImageProcess */

require_once 'lib/processImage.php';
require_once 'amazon.php';

class ImageProcessor extends Api{

	function __construct(){
		parent::__construct();
		global $app_data;
		$this->appdata = $app_data;
	}

	function runProcess(){
		global $params;
		$this->process($params['action']);
	}
	//new

	function process($action){
		global $params;
		$data = $params['data'];

		$this->appdata = App::getAppData($_SESSION['app_id']);
		$path = $this->config['upload_folder'];
		$actionDefinitions = $this->appdata['config']['private']['imageProcesses'][$action]; //Array, az egyes lementett képek instrukciói vannak benne
		for ($j = 0; $j < count($actionDefinitions); $j++) {
			// Egy adott kép legyártása
			$definition = $actionDefinitions[$j];
			// lecseréljük az asset:: elemeket a megadott paraméterekre
			foreach ($definition as &$value) {
				if (is_string($value) && substr($value, 0, 7) == "asset::") {
					$value = $data[substr($value, 7)];
				}
			}

			//Beszívjuk temp file-ra
			$tmp_file = $path.$_SESSION['user']['id'].'_remote_'.time().'_'.md5($definition['image']);
			$original_img = file_get_contents($definition['image']);
			file_put_contents($tmp_file, $original_img);

			$new_file = $_SESSION['user']['id'].'_'.$definition['name'].'_'.md5($tmp_file).'.jpg';
			$image = new SimpleImage();
			$image->load($tmp_file);
			if (isset($definition['resize']) && $definition['resize'] === true) {
				if (isset($definition['crop']) && $definition['crop'] === true) {
					$image->smartCrop($definition['width'],$definition['height']);
				} else {
					$image->smartResize($definition['width'],$definition['height']);
				}
			}

			//Resize megvan, rakjuk rá a komponenseket
			if (isset($definition['components'])) {
				$findParams = array(
					":user_id"=>$params['user_id'],
					":user_name"=>$params['session_user']['name'],
					":current_dir"=>getcwd()
				);
				for ($k = 0; $k < count($definition['components']); $k++) {
					$componentDefinition = $definition['components'][$k];
					//itt is lecseréljük az asset::paramétereket

					foreach ($componentDefinition as $componentkey => &$componentvalue) {
						foreach ($findParams as $paramKey => $paramValue) {
							$pos = strrpos($componentvalue, $paramKey);
							if(is_numeric($pos)){
								$componentvalue = str_replace($paramKey,$paramValue,$componentvalue);
							}
						}		
					}
					//és átadjuk az image file-nak
					$image->applyComponent($componentDefinition);
				}
			}

			//és elmentjük
			$image->save($tmp_file, IMAGETYPE_JPEG, 80);
			$this->Amazon = new Amazon();
			$result = $this->Amazon->uploadBucket(
				array(
					'Key' => "assets/user/" . $new_file,
    				'SourceFile' => $tmp_file,
    				'ContentType' => 'image/jpg'
    			));
			/* return data */
			unlink($tmp_file);
			$return[$definition['name']] = $result;
		}
		$this->return_json($return);
	}

}
?>