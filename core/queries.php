<?php
/* KriekApps API Custom Queries Module */

//require_once 'api.php';

class Queries extends Api{

	function run(){
		global $params;
		$this->runQuery($params['id']);
	}

	function runExport(){
		global $params;
		$this->export($params['query_id']);
	}
	//new

	function publicRun(){
		global $params;

		$query = $this->getQuery($params['id']);
		if($query['public'] && $params['app_id'] == $query['app_id']){
			$this->runQuery($params['id']);
		} else {
			$this->return_error('This query is not public');
		}
	}

	function getQuery($id){
		$sql = "SELECT * FROM queries WHERE id=:id";
		try {
			$db = $this->getConnection();
			$stmt = $db->prepare($sql);  
			$stmt->bindParam("id", $id);
			$stmt->execute();
			$query = $stmt->fetch(PDO::FETCH_ASSOC);
			if($query){
				$query = array_merge($query, json_decode($query['data'],true));
				unset($query['data']);				
			} else {
				$this->return_error("Invalid query ID");
			}
			$db = null;
			return $query;
		} catch(PDOException $e) {
			$this->return_error($e->getMessage());
		}
	}

	function runQuery($id,$return_json = true){
		$query = $this->getQuery($id);
		$sql = $query['query'];

//
		global $params;
		$insert = array();
		foreach ($params as $key => $value) {
			$pos = strrpos($sql." ", ":".$key." ");
			if ($pos === false) {} else {
				if(is_array($value)) {
					$value = json_encode($value);
				}
			    $insert[":".$key] = $value;
			}
		}
//
		
		try {
			$db = $this->getConnection();
			$stmt = $db->prepare($sql);  
			$stmt->execute($insert);
			$data = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			if($data){
				foreach($data as &$row){
					foreach($query['jsonFields'] as $json_field){
						$row[$json_field['column']] = json_decode($row[$json_field['column']],true);

						foreach ($json_field['fields'] as $req_field ){
							if(strpos($req_field,"::")){
								$path = explode("::",$req_field);
								$tmp = $row[$json_field['column']];
								foreach($path as $sub_path){
									$tmp = $tmp[$sub_path];
								}
								$row['tmp'][$req_field] = $tmp;
							} else {
								$row['tmp'][$req_field] = $row[$json_field['column']][$req_field];
							}
						}

						$row = array_merge($row,$row['tmp']);
						unset($row[$json_field['column']],$row['tmp']);
					}
				}			
			} 

			$db = null;
			if($return_json){
				$this->return_json(array("titles" => $query['titles'],  "data" => $data));
			} else {
				return $data;
			}
		} catch(PDOException $e) {
			$this->return_error($e->getMessage());
		}	
	}

	function export($query_id){
		require_once 'lib/PHPExcel.php';

		$data = $this->runQuery($query_id,false);
		$query = $this->getQuery($query_id);

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		$rowCount = 1;
		foreach($data as $row){
			$columnLetter = "A";
			foreach($row as $column){
				$objPHPExcel->getActiveSheet()->setCellValueExplicit($columnLetter.$rowCount, $column, PHPExcel_Cell_DataType::TYPE_STRING);
				$columnLetter++;
			}
		    $rowCount++;
		}
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$filename = $query['name'].'_'.date("Y-m-d",time()).'.xlsx';
		$filepath = './temp_upload/'. $filename;
		// We'll be outputting an excel file
		header('Content-type: application/vnd.ms-excel');

		// It will be called file.xls
		header('Content-Disposition: attachment; filename="'.$filename);

		// Write file to the browser
		$objWriter->save($filepath);
		readfile($filepath);
		unlink($filepath);
	}

}

?>