<?php
/* KriekApps API User Module */

class User extends Api{
//new
	function putUser(){
		global $params;
		if(!$params['user_email']) $params['user_email'] = "";
		$temp = $this->userPutMerge($params['user_id'],$params['app_id']);

		if($temp['extra']) {
			$temp['extra'] = array_merge($temp['extra'],$params['extra']);
		} else {
			$temp['extra'] = $params['extra'];
		}

		$params['session_user'] = $temp;
	}

	function userDetails(){
		global $params;
		$temp = $params['user_id'];
		if($params['session_role'] == "admin") {
			$temp = $params['id'];
		}
		$this->getUserDetails($params['app_id'], $temp);
	}

	function liveUsers(){
		global $params;
		if($params['session_role'] == "admin") {
			$params['return_action']['sql']="SELECT app_id,COUNT(DISTINCT(session)) AS q FROM activity WHERE UNIX_TIMESTAMP(activity.date) > UNIX_TIMESTAMP()-120 GROUP BY app_id";
		}
	}

	function deleteDetails(){
		global $params;
		$this->deleteUserDetails($params['app_id'], $params['id'], $params['modules']);
	}
//new

	function registerUser(){
		global $params;

		if(!isset($params['extra'])){
			$reqBody = $this->slim->request()->getBody();
			if($_GET['_method']){
				$reqBody = json_decode($reqBody,TRUE);
			}
			$params['extra'] = $reqBody['extra'];
		}


		$user = $_SESSION['user'];
		$user['extra'] = $params['extra'];

		$sql = "SELECT count(*) as registered FROM users WHERE id=:user_id AND app_id=:app_id";
		try {
			$db = $this->getConnection();
			$stmt = $db->prepare($sql);  
			$stmt->bindParam("user_id", $user['id']);
			$stmt->bindParam("app_id", $_SESSION['app_id']);
			$stmt->execute();
			$data = $stmt->fetch(PDO::FETCH_ASSOC);
			$db = null;


			/* Checking if user already exists */
			if($data['registered'] == 0){
				
				/* Adding user to DB */
				//$sql = "INSERT INTO users (id, app_id, name, email, data) VALUES (:user_id, :app_id, :name, :email, :data)";
				$sql = "INSERT INTO users (id, app_id, name, email, data, `group`) VALUES (:user_id, :app_id, :name, :email, :data, :group)";
				if(isset($_COOKIE['kriekapp_group'])) {
					$group = $_COOKIE['kriekapp_group'];
				} else {
					$group = 0;
				}

				try {
					$db = $this->getConnection();
					$stmt = $db->prepare($sql);
					$stmt->bindParam("user_id", $user['id']);
					$stmt->bindParam("app_id", $_SESSION['app_id']);
					$stmt->bindParam("name", $user['name']);
					$stmt->bindParam("email", $user['email']);
					$stmt->bindParam("data", json_encode($user));
					$stmt->bindParam("group", $group);
					$stmt->execute();
					$db = null;


					$sql = "UPDATE activity SET user_id=:user_id,date=date WHERE app_id=:app_id AND user_id=0 AND session=:session";
					$this->DB->runSQL($sql,array(
						"session"=>session_id(),
						"user_id"=>$user['id'],
						"app_id"=>$_SESSION['app_id']
					));

					return true;

				} catch(PDOException $e) {
					// $this->return_error($e->getMessage(). " user registration user: ".$user['id']. "  app: ".$_SESSION['app_id'] );
				}
			}
 
		} catch(PDOException $e) {
			$this->return_error($e->getMessage(). " pre user registration");
		}
	}


	function userPutMerge($user,$app_id){
			$sql = "SELECT * FROM users WHERE id=:user_id AND app_id=:app_id";
			try {
				$db = $this->getConnection();
				$stmt = $db->prepare($sql);
				$stmt->bindParam("user_id", $user);
				$stmt->bindParam("app_id", $app_id); 
				$stmt->execute();
				$data = $stmt->fetch(PDO::FETCH_ASSOC);
				$db = null;

				if($stmt->rowCount() > 0){
					return json_decode($data['data'],TRUE);
				} else {
					$user = new User();
					$user->registerUser();
					$this->slim->stop();
				}
	 
			} catch(PDOException $e) {
				$this->return_error($e->getMessage());
			}

	}


// get user details by modules

	function deleteUserDetails($app_id,$user_id,$modules){
			global $app_data;

			if(!isset($app_data['config']['public']['production']['value'])) {
				$app_data['config']['public']['production']['value']= false;
			}

			if($app_data['config']['public']['production']['value']) {
				$this->return_error("This feature is not allowed in production mode");
			}

			$sql = array();
			$insert = array(
				":app_id"=>$app_id,
				":user_id"=>$user_id
			);
			$tables = array(
				"user"=>"DELETE FROM users WHERE id=:user_id AND app_id=:app_id",
				"ugc"=>"DELETE FROM ugc_entries WHERE user_id=:user_id AND app_id=:app_id",
				"votes"=>"DELETE FROM ugc_votes WHERE user_id=:user_id AND app_id=:app_id",
				"sweepstake"=>"DELETE FROM sweepstake_results WHERE user_id=:user_id AND app_id=:app_id",
				"custom"=>"DELETE FROM custom WHERE user_id=:user_id AND app_id=:app_id",
				"invites"=>"DELETE FROM invites WHERE inviter_id=:user_id AND app_id=:app_id",
				"invite"=>"DELETE FROM invites WHERE inviter_id=:user_id AND app_id=:app_id",
				"limits"=>"DELETE FROM limits WHERE user_id=:user_id AND app_id=:app_id",
				"quiz"=>"DELETE FROM quiz_results WHERE user_id=:user_id AND app_id=:app_id",
				"activity"=>"DELETE FROM activity WHERE user_id=:user_id AND app_id=:app_id",
				"payment"=>"DELETE FROM payment_log WHERE user_id=:user_id AND app_id=:app_id"
			);

			foreach ($modules as $key => $value) {
				if($tables[$value]) {
					array_push($sql,$tables[$value]);
					if($value == "ugc"){
						array_push($sql, $tables['votes']);
					}
				}
			}

			foreach ($sql as $key => $value) {
				try {
					$db = $this->getConnection();
					$stmt = $db->prepare($value);
					$stmt->execute($insert);
					$db = null;
		 
				} catch(PDOException $e) {
					$this->return_error($e->getMessage());
				}
			}

			$this->slim->stop();
	}

	function isUserBanned(){
		$sql = "SELECT enabled FROM users WHERE id=:user_id AND app_id=:app_id";
		$data = $this->DB->runSQL($sql,null,'model');
		if(!$data['enabled']) {
			$this->return_error("User is banned from this app");
		}
	}

	function limitActivity(){
		global $params;
		if($params['id'] == 0) {
			$params['return_action']['sql'] .= " LIMIT 10";
		}
	}

	function getUserActivity(){
		global $params;

		$sql = "SELECT activites.ref,activity.ref AS id,activites.collection FROM activites LEFT JOIN activity ON activity.type = activites.id WHERE activity.id=:id";
		$data = $this->DB->runSQL($sql,null,'model');
		if($data['ref'] == "") {
			return;
		} else {
			$params['id'] = $data['id'];
			$params['return_action']['sql'] = $data['ref'];

			if($data['collection']) {
				$params['return_settings']['type'] = "collection";
			}
		}
	}

	function getUserDetails($app_id,$user_id){
		global $app_data;
		global $params;
		
		$modules = $app_data['config']['public']['modules'];

		$sql = array("user"=>"SELECT * FROM users WHERE id=:user_id AND app_id=:app_id");
		$result = array();
		$insert = array(
			":app_id"=>$app_id,
			":user_id"=>$user_id
		);
		$tables = array(
			"ugc"=>"SELECT * FROM ugc_entries WHERE user_id=:user_id AND app_id=:app_id",
			"votes"=>"SELECT * FROM ugc_votes WHERE user_id=:user_id AND app_id=:app_id",
			"sweepstake"=>"SELECT * FROM sweepstake_results WHERE user_id=:user_id AND app_id=:app_id",
			"custom"=>"SELECT * FROM custom WHERE user_id=:user_id AND app_id=:app_id",
			"invites"=>"SELECT * FROM invites WHERE inviter_id=:user_id AND app_id=:app_id",
			"invite"=>"SELECT * FROM invites WHERE inviter_id=:user_id AND app_id=:app_id",
			"limits"=>"SELECT * FROM limits WHERE user_id=:user_id AND app_id=:app_id",
			"activity"=>"SELECT activity.id,activity.app_id,activity.user_id,activites.id as activity_type_id,activites.name_feed AS type,activity.ref,activity.session,activity.date FROM activity LEFT JOIN activites ON activites.id=activity.type WHERE user_id=:user_id AND app_id=:app_id",
			"quiz"=>"SELECT r.*,q.question,q.answers FROM quiz_results AS r LEFT JOIN quiz_questions AS q ON r.question_id=q.id WHERE r.user_id=:user_id AND r.app_id=:app_id",
			"payment"=>"SELECT * FROM payment_log WHERE user_id=:user_id AND app_id=:app_id"
		);

		foreach ($modules as $key => $value) {
			if($value['active'] && isset($tables[$key])) {
				$sql[$key] = $tables[$key];
			}
		}

		if($modules['ugc']['active'] == "true"){
			$sql['votes'] = $tables['votes'];
		}

		if($params['session_role'] == "admin") {
			$sql['activity'] = $tables['activity'];
		}

		foreach ($sql as $key => $value) {
			$result[$key] = $this->retriveData($value,$insert);
		}

		//print_r($sql);
		$this->return_json($result);
		$this->slim->stop();
	}

	function retriveData($sql,$insert){
			try {
				$db = $this->getConnection();
				$stmt = $db->prepare($sql);
				$stmt->execute($insert);
				$data = $stmt->fetchAll(PDO::FETCH_ASSOC);
				$db = null;

				foreach ($data as &$row) {
					foreach ($row as &$cell) {
						$temp = json_decode($cell,TRUE);
						if($temp != null) { $cell = $temp; }
					}
				}

				return $data;
	 
			} catch(PDOException $e) {
				$this->return_error($e->getMessage());
			}

	}


}