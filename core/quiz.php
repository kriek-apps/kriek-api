<?php

/*
KriekApps API Quiz Module
0.1b
*/

//require_once 'api.php';

class Quiz extends Api{
	function addAnsw(){
		global $params;

		if($params['id'] == '0') {
			$result = array();
			foreach ($params['answers'] as $value) {
				$question = $this->getQuestion($value['id'],$params['app_id']);
				$result[$value['id']] = $this->validateAnswer($value['answer'], $question['correct'], $question['question']['type']);
				$sql="INSERT INTO quiz_results (app_id, question_id, user_id, data, correct) VALUES (:app_id ,:id ,:user_id ,:answer ,:correct)";
				$this->DB->runSQL($sql,array("id"=>$value['id'],"answer"=>$value['answer'],"correct"=>$result[$value['id']]));
			}
			$this->quizMessage(array("success","Answers are successfully saved",$result));
		} else {
			$question = $this->getQuestion($params['id'],$params['app_id']);
			$params['correct'] = $this->validateAnswer($params['answer'], $question['correct'], $question['question']['type']);
			//$params['correct'] = ($params['correct']) ? 'true' : 'false';
		}
	}

	function quizMessage($msg){
		$this->return_message(array("status" => $msg[0], "message" => $msg[1], "correct" => $msg[2]));
	}
//new

	function getQuestion($question_id,$app_id){
		$sql = "SELECT * FROM quiz_questions WHERE id=:question_id AND app_id=:app_id";

		try {
			$db = $this->getConnection();
			$stmt = $db->prepare($sql);  
			$stmt->bindParam("question_id", $question_id);
			$stmt->bindParam("app_id", $app_id);
			$stmt->execute();
			$data = $stmt->fetch(PDO::FETCH_ASSOC);

			$db = null;

			if($data){
				$data['question'] = json_decode($data['question'],true);
				$data['answers'] = json_decode($data['answers'],true);

				return $data;
			} else {
				$this->return_error("Invalid question id");
			}

		} catch(PDOException $e) {
			$this->return_error($e->getMessage());
		}
	}

	function validateAnswer($answer, $correct_answer, $type){
		if($type == 'radio'){
			return ($answer == $correct_answer ? true : false);
		} elseif($type == 'text'){

//text $correct_answer has to be json field in the database!!
			foreach(json_decode($correct_answer) as $correct){
				$answer = preg_replace('/[^a-z0-9]/', "", strtolower($answer));
				if($answer == $correct) return true;
			}
			return false;
		}
	}

}

?>