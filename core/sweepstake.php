<?php
/*
KriekApps API Sweepstake Module
0.1b
*/

//require_once 'api.php';

class Sweepstake extends Api{
	// new sweepstakes >>>>
	function drawUserFromLimits(){
		global $app_data;

		$briefcase = $app_data['briefcase'];

		$sql="INSERT INTO limits (app_id,user_id,data,type) VALUES (:app_id,:user_id,'','sweepstake')";
		$limits=$this->DB->runSQL($sql);

		$briefKey;
		foreach ($briefcase as $key => $value) {
			if($value['shorthand'] == 'sweepstakes'){
				$prizes = $value['contents'];
				$briefKey = $key;
				break;
			}
		}

		$prizesIndex;
		foreach ($briefcase as $key => $value) {
			if($value['shorthand'] == 'prizes'){
				$gifts = $value['contents'];
				$prizesIndex = $key;
				break;
			}
		}

		$isWinner = false;
		foreach ($prizes as $key => $value) {	
			if($value['available'] == 1 && strtotime($value['date'])<=time()){
				foreach ($briefcase[$prizesIndex]['contents'] as $key2 => $value2) {
					if($value2['id'] == $value['prize']){
						$giftKey = $key2;
					}
				}
				
				$sql="UPDATE limits SET data=:id, data2=:data2 WHERE id=:lastInsertId AND 1=(SELECT true FROM (SELECT * FROM limits WHERE app_id=:app_id AND data=:id LIMIT 1) AS a) IS NULL AND :lastInsertId =(SELECT b.id FROM (SELECT * FROM limits WHERE app_id=:app_id AND date>=:draw_date AND data='' ORDER BY id ASC LIMIT 1) AS b)";
				$result=$this->DB->runSQL($sql,array("id"=>$value['id'],"draw_date"=>$value['date'],"lastInsertId"=>$limits['last_id'], "data2"=>json_encode($briefcase[$prizesIndex]['contents'][$giftKey])));
				if($result['affected_rows']!=0) {
					$briefcase[$briefKey]['contents'][$key]['available'] = 0;
					$sql="UPDATE apps SET briefcase=:briefcase WHERE id=:app_id";
					$result=$this->DB->runSQL($sql,array("briefcase"=>$briefcase));
					$this->activityFeed(array(12,$limits['last_id']));
					$this->memcache->flush();
					$this->return_json(array("status" => "success", "id"=>$limits['last_id'], "message" => "Sweepstake successfully saved", "won" => true, "prize" => $briefcase[$prizesIndex]['contents'][$giftKey]));
					$isWinner = true;
					break;
				}
			}
		}
		if(!$isWinner) {
			$sql="UPDATE limits SET data=0 WHERE id=:lastInsertId";
			$this->DB->runSQL($sql,array("lastInsertId"=>$limits['last_id']));
			$this->activityFeed(array(12,$limits['last_id']));
			$this->return_json(array("status" => "success", "id"=>$limits['last_id'], "message" => "Sweepstake successfully saved", "won" => false));
		}

	}

	// new sweepstakes <<<<
	/* Check if user can play */
	function canUserPlay($allowed){
		$sql = "SELECT count(user_id) as times FROM sweepstake_results WHERE date >= :fromdate AND date < :tilldate AND user_id=:user_id";
		try {
			$db = $this->getConnection();
			$stmt = $db->prepare($sql);  
			$stmt->bindParam("user_id", $_SESSION['user']['id'],PDO::PARAM_INT);
			$stmt->bindParam("fromdate", date('Y-m-d'), PDO::PARAM_STR);
			$stmt->bindParam("tilldate", date('Y-m-d',strtotime('tomorrow')),PDO::PARAM_STR);
			$stmt->execute();
			$result = $stmt->fetch(PDO::FETCH_ASSOC);
			$db = null;

			if($result['times'] >= $allowed){
				return false;
			} else {
				return true;
			}

		} catch(PDOException $e) {
			$this->return_error($e->getMessage());
		}
	}

	function getNumberofTodayWinners(){
		$db = $this->getConnection();
		$sql = "SELECT count(user_id) as times FROM sweepstake_results WHERE DATE(`date`) = DATE(NOW()) AND won = 1 AND app_id=:app_id";
		$stmt = $db->prepare($sql);
		$stmt->bindParam("app_id", $_SESSION['app_id'],PDO::PARAM_STR);
		$stmt->execute();
		$db = null;
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		return $result['times'];
	}

	/* Save Sweepsteak data */
	function addResult(){
		global $app_data;

		//Get result data
		$data = $this->decryptData($this->slim->request()->params('data'));

		//Validate Sweepstake data
		if(date('d') != $data['day'] || session_id() != $data['session_id']){
			$this->return_error('Invalid sweepstake data');
		}

		if(!$this->canUserPlay( $app_data['config']['public']['modules']['sweepstake']['max_play_per_user_per_day'] )){
			$this->return_json(array("status" => "success", "message" => "Played already", "won" => 0));
			$this->slim->stop();
		}

		if($this->getNumberofTodayWinners() > $app_data['config']['public']['modules']['sweepstake']['max_winners_per_day'] - 1 && $data['won'] = 1){
			$data['won'] = 0;
			$data['reason'] = $app_data['config']['public']['modules']['sweepstake']['max_winners_per_day']. " winners reached";
		}


		$sql = "INSERT INTO sweepstake_results (app_id, user_id, data, won) VALUES (:app_id, :user_id, :data, :won)";
		try {
			$db = $this->getConnection();
			$stmt = $db->prepare($sql);  
			$stmt->bindParam("user_id", $_SESSION['user']['id']);
			$stmt->bindParam("app_id", $_SESSION['app_id']);
			$stmt->bindParam("data", json_encode($data));
			$stmt->bindParam("won", $data['won']);
			$stmt->execute();
			$this->activityFeed(array(12,$db->lastInsertId()));
			$db = null;

			$this->return_json(array("status" => "success", "message" => "Sweepstake successfully saved", "won" => (int)$data['won']));

		} catch(PDOException $e) {
			$this->return_error($e->getMessage());
		}
		
	}

}

?>