<?php
/*
KriekApps API Facebook Module
0.1b
*/

require_once 'lib/Facebook/facebook.php';

class FB extends Api{

	function __construct(){
		parent::__construct();
		$this->fbinit();
	}

	function fbinit(){
		global $app_data;

		$this->fb = new Facebook(array(
			'appId'  => $_SESSION['app_id'],
			'secret' => $app_data['config']['private']['app_secret']
		));	
	}

	function fbapi($path = '/me?fields=name,email,id,verified,gender,first_name,last_name,link,timezone,updated_time',$method = 'GET',$extra = array()){
		try {
		    return $this->fb->api($path,$method, $extra);
		  } catch (FacebookApiException $e) {
		  	$this->return_error($e->getMessage());
		}
	}

	function getCurrentFBUser(){
		return $this->fbapi('/me?fields=name,email,id,verified,gender,first_name,last_name,link,timezone,updated_time','GET',array(
			"access_token" => $_SESSION['access_token']
		));
	}

	function Reminder(){
		global $params;
		global $app_data;

		$res = $this->fbapi('/'.$params['user_id'].'/notifications','POST',array(
			"template" => $params['message'],
			"access_token" => $app_data['config']['private']['app_token']['value'],
			"href" => "?reminder=true"
		));

		$this->activityFeed(array(16));		

		$this->return_json(array("status" => "success", "message" => "Reminder has been sent"));
	}

	function redirectToFacebookTab(){
		global $app_data;
		list($encoded_sig, $payload) = explode('.', $_POST['signed_request'], 2); 

		$secret = $app_data['config']['private']['app_secret']['value']; // Use your app secret here

		// decode the data
		$sig = $this->base64_url_decode($encoded_sig);
		$data = json_decode($this->base64_url_decode($payload), true);

		// confirm the signature
		$expected_sig = hash_hmac('sha256', $payload, $secret, $raw = true);
		if ($sig !== $expected_sig) {
			$this->return_error("Bad Signed JSON signature!");
		}

		$signed = array(
			"liked" => $data['page']['liked'],
			"page" => $data['page']['id'],
			"country" => $data['user']['country'],
			"app_data" => $data['app_data']
		);

		$this->slim->getInstance()->redirect($app_data['config']['public']['app_url']['value']."#".urlencode(json_encode($signed)));

	}

	function base64_url_decode($input) {
	  return base64_decode(strtr($input, '-_', '+/'));
	}

}