<?php

/* Slim Framework */
require_once 'lib/Slim/Slim.php';

/* Config */
require_once 'config/config.php';

/* Client Auth Class */
require_once 'client_auth.php';

/* Auth */
require_once 'auth.php';

/* App Class */
require_once 'app.php';

/* Facebook Class */
require_once 'facebook.php';

/* User Class */
require_once 'user.php';

/* DATABASE Class */
require_once 'db.php';

/* DATABASE Class */
require_once 'payment.php';

/* Memcache Class */
require_once 'memcacheapi.php';

/* Draw Class */
//require_once 'draw.php';

/* Custom Queries Class */
//require_once 'queries.php';

/* App related Classes */
	/* Quiz */
	//require_once 'quiz.php';

	/* UGC */
	//require_once 'ugc.php';

	/* ImageProcessor */
	//require_once 'imageProcessor.php';

	/* Sweepstake Class */
	//require_once 'sweepstake.php';

	/* Invite Class */
	//require_once 'invite.php';
	
/* Endpoint Class */
require_once 'endpoint.php';

class Api{

	function __construct(){
		/* Init Slim Framework */
		$this->init_slim();

		/* Config */
		global $config;
		$this->config = $config;
		$this->DB = new DB();
		$this->memcache = $this->setupMemcache();
	}

	function init_slim(){
		\Slim\Slim::registerAutoloader();
		$this->slim = new \Slim\Slim();
		$this->slim->add(new \Slim\Middleware\ContentTypes());
	}

	//memcache
	function setupMemcache(){
		try{
			$m = new Memcache();
			$m->addServer(MEMCACHED_HOST, MEMCACHED_PORT);
			if($this->config['memcacheExtra']) {
				$m->addServer(MEMCACHED_HOST, MEMCACHED_PORT2);
			}
			return $m;
		} catch(Exception $e) {
			$this->return_error($e->getMessage());
		}
	}

	function setMemcacheValue($key, $value){
		$keys = '@keys';
		$mem = $this->memcache->get($keys);
		if($mem == "") {
			$mem = array($keys);
		}

		if(!isset($mem[$key])){
			$mem[$key] = $key;
		}

		$this->memcache->set($keys, $mem);
		$this->memcache->set($key, $value);
	}
	//memcache

	function return_json($array){
		global $params;
		$res = $this->slim->getInstance()->response();
		$res['Content-Type'] = 'application/json';
		$res['X-Powered-By'] = 'Canvas';


		$json = preg_replace('/:\s?(\d{14,})/', ': "${1}"',json_encode($array, JSON_NUMERIC_CHECK));

		if(isset($params['memcache'])) {
			$memcache = new MemcacheApi();
			$memcache->syncTable($params['memcache'], $json);
		}

		if(strlen($json) > 100 && !isset($_GET['_method'])) {
			$res['Content-Encoding'] = 'gzip';
			$json = gzencode($json,5);
		}
		
		if($array != "{}") {
			$res->write($json);
		} else {
			echo $array;
		}
		if($this->config['developer']) {
			$this->runtime();
		}
	}

	function return_error($msg){
		$this->logError($msg);
		$res = $this->slim->getInstance()->response();
		$res->status(400);
		$res['Content-Type'] = 'application/json';
		$res['X-Powered-By'] = 'Canvas';

		$return = array("status" => "error", "message" => $msg);
		$res->write(json_encode($return,JSON_NUMERIC_CHECK));
		$this->slim->stop();
	}

	function getConnection() {
		date_default_timezone_set('Europe/Budapest');
		$now = new \DateTime();
		$mins = $now->getOffset() / 60;

		$sgn = ($mins < 0 ? -1 : 1);
		$mins = abs($mins);
		$hrs = floor($mins / 60);
		$mins -= $hrs * 60;

		$offset = sprintf('%+d:%02d', $hrs*$sgn, $mins);

		$dbhost=$this->config['dbhost'];
		$dbuser=$this->config['dbuser'];
		$dbpass=$this->config['dbpass'];
		$dbname=$this->config['dbname'];
		$options = array(
			PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8;'
		);
		$dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass, $options);	
		$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$dbh->exec('SET time_zone="'.$offset.'";');
		return $dbh;
	}

	/* DecryptData data */
	function decryptData($data){
		$key = "vivalakriek";
		$data = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode(strtr( $data, "-_.", "+/=" )), MCRYPT_MODE_CBC, md5(md5($key))), "\0");
		return json_decode($data,true);
	}

	/* Encrypt data */
	function encryptData($data){
		$key = "vivalakriek";
		$data = json_encode($data);
		return strtr(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $data, MCRYPT_MODE_CBC, md5(md5($key)))), "+/=", "-_." );
	}

	/* Convert table datafields to JSON */
	function convertToJSON($jsonFields){
		global $params;
		if(!is_array($params['return_data'][0])) {
			foreach($jsonFields as $field){   
				$params['return_data'][$field] = json_decode($params['return_data'][$field],TRUE);
			}
		} else {
			foreach($params['return_data'] as &$row){
				foreach($jsonFields as $field){   
					$row[$field] = json_decode($row[$field],TRUE);
				}
			}
		}
	}

	/* Convert undefined table datafields to JSON */
	function loopOverToJSON(){
		global $params;
		if(count($params['return_data']) < 2) {
		 	return;
		}
		if(is_array($params['return_data'][0])) {
			foreach($params['return_data'] as &$row){
				foreach($row as &$field){
					$temp = substr($field,0,1);
					if($temp == "{" || $temp == "["){   
						$json = json_decode($field,TRUE);
						if($json != null) {
							$field = $json;
						}
					}
				}
			}			
		} else {
			foreach($params['return_data'] as &$field){
				$temp = substr($field,0,1);
				if($temp == "{" || $temp == "["){   
					$json = json_decode($field,TRUE);
					if($json != null) {
						$field = $json;
					}
				}
			}
		}
	}

	function return_message($return){ 
		$res = $this->slim->getInstance()->response();
		$res->status(200);
		$res['Content-Type'] = 'application/json';
		$res['X-Powered-By'] = 'Canvas';

		if(isset($return[0])) {
			$return = $return[0];
		}

		$json = json_encode($return,JSON_NUMERIC_CHECK);
		if(strlen($json) > 100) {
			$res['Content-Encoding'] = 'gzip';
			$json = gzencode($json,5);
		}

		$res->write($json);
		$this->slim->stop();
	}

	function return_memcache($data){
		$res = $this->slim->getInstance()->response();
		$res->status(200);
		$res['Content-Type'] = 'application/json';
		$res['X-Powered-By'] = 'Canvas';
		$res['X-Served-From'] = 'Memcached';
		$res['Content-Encoding'] = 'gzip';

		$res->write($data);
		$this->slim->stop();
	}

	function stop(){
		$this->slim->stop();
	}

	function loopArray($arr,$name){
		global $params;
		foreach ($arr as $key => $value) {
			if(is_array($value)) {
				$this->loopArray($value,$name.'_'.$key);
			} else {
				$params[$name.'_'.$key]=$value;
			}
		}
	}

	function endpointManager(){
		if(!$this->config['developer'] || $_SESSION['user']['id'] != 1) {
			$this->return_error("restricted access");
		}
	}

	function activityFeed($data){
		global $app_data;

		if(isset($app_data)) {

			if($app_data['config']['public']['archive']['value']) {
				return;
			}

			$d = new DateTime($app_data['config']['public']['active_until']['value']);
			$timestamp = $d->getTimestamp();

			if(time() > $timestamp){
				return;
			}
		}

		global $params;

		if(!isset($params['app_id'])) {
			return;
		}

		$type = $data[0];
		$ref = $data[1];

		if(!isset($ref)) {
			$ref = '';
		}

		$insert = array("type"=>$type,"ref"=>$ref,"id_session"=>session_id());

		if(isset($params['behalf'])) {
			$insert['user_id'] = $params['behalf'];
			$insert['id_session'] = $params['id_session'];
		}

		if(!isset($params['user_id'])) {
			$insert['user_id'] = 0;
		}

		if(isset($params['user_group'])){
			$insert['group'] = $params['user_group'];
		} else if(isset($_COOKIE['kriekapp_group'])){
			$insert['group'] = $_COOKIE['kriekapp_group'];
		} else {
			$insert['group'] = 0;
		}
		$sql = "INSERT INTO activity (app_id,user_id,type,ref,session,`group`) VALUES (:app_id,:user_id,:type,:ref,:id_session,:group)";
		$this->DB->runSQL($sql,$insert,"activity");

	}

	function runtime(){
		// $sql = "INSERT INTO runtime (endpoint,runtime) VALUES (:endpoint,:runtime)";
		// $this->DB->runSQL($sql,array(
		// 	"endpoint"=>str_replace("/api/","",$_SERVER['REDIRECT_URL']),
		// 	"runtime"=>microtime(true)*1000 - $_SERVER['REQUEST_TIME_FLOAT']*1000
		// ));
	}

	//save error in databse
	function clientError(){
		global $params;
		$this->logError($params['error'],"client");
		$this->return_message(array("status" => "success", "message" => "Client error saved"));
	}

	function logError($msg,$origin="server"){
		global $params;

		if(!isset($params['error_counter'])) {
			$params['error_counter'] = "avoid infinite loop error";
		} else {
			return; // infinite loop protection
		}
		$sql = "INSERT INTO errorlog (app_id,message,params,server,session,origin) VALUES (:app_id,:message,:params,:server,:session,:origin)";
		$insert = array(
			":app_id"=>$_SESSION['app_id'],
			":message"=>$msg,
			":params"=>json_encode($this->slim->request()->getBody()),
			":origin"=>$origin
		);

		$server = array(
			"HTTPS"=>$_SERVER['HTTPS'],
			"HTTP_X_REQUESTED_WITH"=>$_SERVER['HTTP_X_REQUESTED_WITH'],
			"HTTP_USER_AGENT"=>$_SERVER['HTTP_USER_AGENT'],
			"CONTENT_TYPE"=>$_SERVER['CONTENT_TYPE'],
			"HTTP_REFERER"=>$_SERVER['HTTP_REFERER'],
			"HTTP_ORIGIN"=>$_SERVER['HTTP_ORIGIN'],
			"REDIRECT_URL"=>$_SERVER['REDIRECT_URL'],
			"REQUEST_METHOD"=>$_SERVER['REQUEST_METHOD']
		);
		$insert[':server'] = json_encode($server);

		$session = array(
			"user"=> array(),
			"role"=>$_SESSION['role']
		);
		$session['user'] = array(
			"name"=> $_SESSION['user']['name'],
			"id"=>$_SESSION['user']['id']
		);
		$insert[':session'] = json_encode($session);


		if($origin=="client") {
			$insert[':params'] = $params['details'];
		}

		try {

			$db = $this->getConnection();
			$stmt = $db->prepare($sql);  
			$stmt->execute($insert);
			$this->activityFeed(array(15,$db->lastInsertId()));

			$db = null;

		} catch(PDOException $e) {
			error_log($e->getMessage());
			die();
		}
	}


}

?>