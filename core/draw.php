<?php
/*
KriekApps API Draw Module
0.1b
*/

//require_once 'api.php';

class Draw extends Api{

	function run(){
		$return = array();
		$draws = $this->getTodayDraws();
		if($draws){
			foreach($draws as $draw){
				$winners = $this->dbFetch($draw['query']);
				if($winners){
					$this->dbInsertWinner($winners, $draw['app_id'], $draw['id']);
					$return[] = array($winners, $draw['app_id'], $draw['id']);
				}
				$this->updateDraw($draw['draw_date_id']);
			}

			$this->return_json($return);
			
		} else {
			$this->return_json(array());
		}
	}


	function updateDraw($id){
		$query = "UPDATE draw_dates SET `drawed` = 1 WHERE id = ".$id;
		try {
			$db = $this->getConnection();
			$stmt = $db->prepare($query); 
			$stmt->execute();
			$db = null;

			return true;
			
		} catch(PDOException $e) {
			return $this->return_error($e->getMessage());
		}
	}

	function dbFetch($query){
		try {
			$db = $this->getConnection();
			$stmt = $db->prepare($query);  
			$stmt->execute();
			$data = $stmt->fetchAll(PDO::FETCH_ASSOC);
			$db = null;

			return $data;
			
		} catch(PDOException $e) {
			return $this->return_error($e->getMessage());
		}			
	}

	function dbInsertWinner($winners,$app_id,$draw_id){
		$query = "INSERT INTO `winners` (`user_id`,`app_id`,`draw_id`) VALUES";
			foreach($winners as $winner){
				$query.= "(".$winner['user_id'].", ".$app_id.", ".$draw_id."),";
			}
		$query = substr($query, 0, -1);
		$query .= ";";

		try {
			$db = $this->getConnection();
			$stmt = $db->prepare($query);  
			$stmt->execute();
			$db = null;
		} catch(PDOException $e) {
			return $this->return_error($e->getMessage());
		}			
	}

	function getTodayDraws(){
		return $this->dbFetch("SELECT query, app_id, draws.id, draw_dates.id AS draw_date_id
		FROM draws
		JOIN draw_dates ON draws.id = draw_dates.draw_id
		WHERE draw_dates.`date` = CURDATE( ) 
		AND draw_dates.drawed =0");
	}

}
?>