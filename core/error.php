<?php
/* KriekApps API User Module */

class Error extends Api{

	function deleteAllAppError(){
		$sql = "DELETE FROM errorlog WHERE app_id=:app_id";
		$response = array("tbl_error"=>$this->DB->runSQL($sql));
		$sql = "DELETE FROM activity WHERE app_id=:app_id AND type=15";
		$response = $response + array("tbl_activity"=>$this->DB->runSQL($sql));
		$this->return_json($response);
	}

}