<?php
require_once 'lib/paypal.class.php';


class Payment extends Api{
	
	function SetExpressCheckout($ItemName,$ItemDesc,$ItemPrice,$ItemNumber,$ItemQty){
		global $app_data;

		// Get Payment settings out from briefcase
		$PaymentSettings = $this->getPaymentSettings();

		$ItemTotalPrice = $ItemPrice * $ItemQty; //(Item Price x Quantity = Total) Get total amount of product; 

		//Data to be sent to paypal
		$padata =	'&PAYMENTREQUEST_0_AMT='.urlencode($ItemTotalPrice).
					'&PAYMENTREQUEST_0_PAYMENTACTION=Sale'.
					'&PAYMENTREQUEST_0_CURRENCYCODE='.urlencode($PaymentSettings['PayPalCurrencyCode']).
					'&RETURNURL='.urlencode($this->config['api_url'].$app_data['id'].'/payment/process?access_token='.$_SESSION['access_token']).
					'&CANCELURL='.urlencode($this->config['api_url'].$app_data['id'].'/payment/cancel').
					'&REQCONFIRMSHIPPING=0'.
					'&NOSHIPPING=1'.

					'&L_PAYMENTREQUEST_0_NAME0='.urlencode($ItemName).
					// '&L_PAYMENTREQUEST_0_DESC0='.urlencode($ItemDesc).
					'&L_PAYMENTREQUEST_0_NUMBER0='.urlencode($ItemNumber).
					'&L_PAYMENTREQUEST_0_AMT0='.urlencode($ItemPrice).
					'&L_PAYMENTREQUEST_0_QTY0='. urlencode($ItemQty).
					'&L_PAYMENTREQUEST_0_ITEMCATEGORY0=Digital';

		//We need to execute the "SetExpressCheckOut" method to obtain paypal token
		$paypal= new MyPayPal();

		// Do API call
		$httpParsedResponseAr = $paypal->PPHttpPost('SetExpressCheckout', $padata, $PaymentSettings['PayPalApiUsername'], $PaymentSettings['PayPalApiPassword'], $PaymentSettings['PayPalApiSignature'], $PaymentSettings['PayPalMode']);

		//Respond according to message we receive from Paypal
		if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"]))
		{
					
				// If successful set some session variable we need later when user is redirected back to page from paypal. 
				$_SESSION['payment']['itemprice'] =  $ItemPrice;
				$_SESSION['payment']['totalamount'] = $ItemTotalPrice;
				$_SESSION['payment']['itemName'] =  $ItemName;
				$_SESSION['payment']['itemNo'] =  $ItemNumber;
				$_SESSION['payment']['ItemQty'] =  $ItemQty;


				if($PaymentSettings['PayPalMode']=='sandbox')
				{
					$paypalmode 	=	'.sandbox';
				}
				else
				{
					$paypalmode 	=	'';
				}

				//Redirect user to PayPal store with Token received.
				$paypalurl ='https://www'.$paypalmode.'.paypal.com/incontext?token='.$httpParsedResponseAr["TOKEN"];
				$response = $this->slim->getInstance()->response();
				$response->header('Location',$paypalurl);
			 
		}else{
			// Show error
			$this->returnState(array("status" => "error", "message" => $httpParsedResponseAr["L_LONGMESSAGE0"]));
		}
	}

	function DoExpressCheckoutPayment(){
		//Paypal redirects back to this page using ReturnURL, We should receive TOKEN and Payer ID
		if(isset($_GET["token"]) && isset($_GET["PayerID"]))
		{

			// Get Payment settings out from briefcase
			$PaymentSettings = $this->getPaymentSettings();
			
			$token = $_GET["token"];
			$payerId = $_GET["PayerID"];
			
			//get session variables
			$ItemPrice 		= $_SESSION['payment']['itemprice'];
			$ItemTotalPrice = $_SESSION['payment']['totalamount'];
			$ItemName 		= $_SESSION['payment']['itemName'];
			$ItemNumber 	= $_SESSION['payment']['itemNo'];
			$ItemQty 		= $_SESSION['payment']['ItemQty'];

			
			$padata = 	'&TOKEN='.urlencode($token).
								'&PAYERID='.urlencode($payerId).
								'&PAYMENTACTION='.urlencode("SALE").
								'&AMT='.urlencode($ItemTotalPrice).
								'&CURRENCYCODE='.urlencode($PaymentSettings['PayPalCurrencyCode']);
			
			//We need to execute the "DoExpressCheckoutPayment" at this point to Receive payment from user.
			$paypal= new MyPayPal();
			$httpParsedResponseAr = $paypal->PPHttpPost('DoExpressCheckoutPayment', $padata, $PaymentSettings['PayPalApiUsername'], $PaymentSettings['PayPalApiPassword'], $PaymentSettings['PayPalApiSignature'], $PaymentSettings['PayPalMode']);
			
			
			//Check if everything went ok..
			if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) 
			{
					$transactionID = urlencode($httpParsedResponseAr["TRANSACTIONID"]);
					$nvpStr = "&TRANSACTIONID=".$transactionID;
					$httpParsedResponseAr = $paypal->PPHttpPost('GetTransactionDetails', $nvpStr, $PaymentSettings['PayPalApiUsername'], $PaymentSettings['PayPalApiPassword'], $PaymentSettings['PayPalApiSignature'], $PaymentSettings['PayPalMode']);
					
					// store data to DB $httpParsedResponseAr
					$insert_sql = "INSERT INTO payment_log (app_id, user_id, product_id, payment_data, amount, success) VALUES (:app_id, :user_id, :product_id, :payment_data, :amount, 1)";
					$response = $this->DB->runSQL($insert_sql, array(
						"product_id" => $ItemNumber,
						"payment_data" => json_encode($httpParsedResponseAr),
						"amount" => $ItemTotalPrice
					));

					$this->activityFeed(array(17,$response['last_id']));

					$this->returnState($response);
			
			}else{
					// store data to DB $httpParsedResponseAr
					$insert_sql = "INSERT INTO payment_log (app_id, user_id, product_id, payment_data, success) VALUES (:app_id, :user_id, :product_id, :payment_data, 0)";
					$this->DB->runSQL($insert_sql, array(
						"product_id" => $ItemNumber,
						"payment_data" => json_encode($httpParsedResponseAr)
					));
					
					$this->returnState(array("status" => "error", "message" => urldecode($httpParsedResponseAr["L_LONGMESSAGE0"])));
			}
		}	else {
			$this->returnState(array("status" => "error", "message" => "Paypal Token or PayerID is missing"));
		}	
	}

	function getPaymentSettings(){
		global $app_data;
			
		// Get Products From Briefcase
		$briefcase = $app_data['briefcase'];
		// $briefcase = json_decode($app_data['briefcase'],true);
		$settings = array();
		$FormattedSettings = array();

		// Find item with ID
		for($i = 0; $i <= count($briefcase); $i++){
			if($briefcase[$i]['shorthand'] == "payment"){
				$settings = $briefcase[$i]['contents'];
			}
		}

		// Clean data
		foreach ($settings as $key => $value) {
			$FormattedSettings[$key] = $value['value'];
		}

		// return data or error if definition is missing
		if(count($FormattedSettings) > 4){
			return $FormattedSettings;
		} else {
			$this->returnState(array("status" => "error", "message" => "There is no \"payment\" shorthanded definition in Briefcase or there are missing fields in it"));
		}


	}

	function returnState($data){
		echo "<script>
			document.domain = \"cnvs.io\";
			window.opener.dg.Response(".json_encode($data).");
			window.opener.dg.closeFlow();
			</script>";
	}

}

?>