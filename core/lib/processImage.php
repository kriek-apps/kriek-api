<?php
 
/*
* File: SimpleImage.php
* Author: Simon Jarvis
* Copyright: 2006 Simon Jarvis
* Date: 08/11/06
* Link: http://www.white-hat-web-design.co.uk/articles/php-image-resizing.php
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details:
* http://www.gnu.org/licenses/gpl.html
*
*/
 
class SimpleImage {
 
   var $image;
   var $image_type;
 
   function load($filename) {
 
      $image_info = getimagesize($filename);
      $this->image_type = $image_info[2];
      if( $this->image_type == IMAGETYPE_JPEG ) {
 
         $this->image = imagecreatefromjpeg($filename);
      } elseif( $this->image_type == IMAGETYPE_GIF ) {
 
         $this->image = imagecreatefromgif($filename);
      } elseif( $this->image_type == IMAGETYPE_PNG ) {
 
         $this->image = imagecreatefrompng($filename);
      }
   }
   function save($filename, $image_type=IMAGETYPE_JPEG, $compression=100, $permissions=null) {
 
      if( $image_type == IMAGETYPE_JPEG ) {
         imagejpeg($this->image,$filename,$compression);
      } elseif( $image_type == IMAGETYPE_GIF ) {
 
         imagegif($this->image,$filename);
      } elseif( $image_type == IMAGETYPE_PNG ) {
 
         imagepng($this->image,$filename);
      }
      if( $permissions != null) {
 
         chmod($filename,$permissions);
      }
   }
   function output($image_type=IMAGETYPE_JPEG) {
 
      if( $image_type == IMAGETYPE_JPEG ) {
         imagejpeg($this->image);
      } elseif( $image_type == IMAGETYPE_GIF ) {
 
         imagegif($this->image);
      } elseif( $image_type == IMAGETYPE_PNG ) {
 
         imagepng($this->image);
      }
   }
   function getWidth() {
 
      return imagesx($this->image);
   }
   function getHeight() {
 
      return imagesy($this->image);
   }
   function resizeToHeight($height) {
 
      $ratio = $height / $this->getHeight();
      $width = $this->getWidth() * $ratio;
      $this->resize($width,$height);
   }
 
   function resizeToWidth($width) {
      $ratio = $width / $this->getWidth();
      $height = $this->getheight() * $ratio;
      $this->resize($width,$height);
   }
 
   function scale($scale) {
      $width = $this->getWidth() * $scale/100;
      $height = $this->getheight() * $scale/100;
      $this->resize($width,$height);
   }

   function smartCrop($width,$height){
      if($width > $height){
         
         $percent = ($width / $this->getWidth()) * 100;
         $this->scale($percent);

         $this->crop($width, $height);

      } else if( $height > $width ){

         $percent = ($height / $this->getheight()) * 100;
         $this->scale($percent);

         $this->crop($width, $height);

      } else {
         if($this->getheight() > $this->getWidth()){
            
            $percent = ($width / $this->getWidth()) * 100;
            $this->scale($percent);

            $this->crop($width, $width);

         } else if( $this->getheight() < $this->getWidth() ){

            $percent = ($height / $this->getheight()) * 100;
            $this->scale($percent);

            $this->crop($height, $height);

         } else {
            $this->resize($width, $height);
         }
      }
   }

   function smartResize($width,$height){
      if($height < $this->getheight() && $width > $this->getWidth()){
         $this->resizeToHeight($height);
      } else if( $width < $this->getWidth() ){
         $this->resizeToWidth($width);
      }
   }

   function crop($width, $height){
      $new_image = imagecreatetruecolor($width, $height);
      $src_x = ($this->getWidth() - $width) / 2;
      $src_y = ($this->getHeight() - $height) / 2;
      imagecopyresampled($new_image, $this->image, 0, 0, $src_x, $src_y, $width, $height, $width, $height);
      $this->image = $new_image;      
   }

   function cropXY($x, $y, $width, $height){
      $new_image = imagecreatetruecolor($width, $height);
      imagecopyresampled($new_image, $this->image, 0, 0, abs($x), abs($y), $width, $height, $width, $height);
      $this->image = $new_image;      
   }
 
   function resize($width,$height) {
      $new_image = imagecreatetruecolor($width, $height);
      imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
      $this->image = $new_image;
   }

   function applyComponent($params) {
      if ($params['type'] == "image") {
         $this->watermark($params);
      }
      if ($params['type'] == "text") {
         $this->text($params);
      }

   }

   function watermark($settings) {
      $im = $this->image; //background image
      $stamp = new SimpleImage(); //component
      $stamp->load($settings['src']);
      //Resizeoljuk, cropoljuk, ha kell
      if (isset($settings['resize']) && $settings['resize'] == true) {
            if (isset($settings['crop']) && $settings['crop'] == true) {
               $stamp->smartCrop($settings['width'],$settings['height']);
            } else {
               $stamp->smartResize($settings['width'],$settings['height']);
            }
      }
      // Pozíciók átállítása corner szerint
      if ($settings['posX'] === "auto") {
         $settings['posX'] = ( $this->getWidth() - $stamp->getWidth() ) / 2;
      } else {
         if (isset($settings['corner'])) {
            //Modify position parameters according to corner
            if ($settings['corner'] == "top-right") {
               $settings['posX'] = $this->getWidth() - $stamp->getWidth() - $settings['posX'];
            }

           if ($settings['corner'] == "bottom-right") {
               $settings['posX'] = $this->getWidth() - $stamp->getWidth() - $settings['posX'];
            }
         }

      }

      if ($settings['posY'] === "auto") {
         $settings['posY'] = ( $this->getHeight() - $stamp->getHeight()) / 2;
      } else {
         if (isset($settings['corner'])) {

            if ($settings['corner'] == "bottom-left") {
               $settings['posY'] = $this->getHeight() - $stamp->getHeight() - $settings['posY'];
            }

            if ($settings['corner'] == "bottom-right") {
               $settings['posY'] = $this->getHeight() - $stamp->getHeight() - $settings['posY'];
            }
         }
      }




      imagecopy($im, $stamp->image, $settings["posX"],  $settings["posY"], 0, 0, imagesx($stamp->image), imagesy($stamp->image));
      $this->image = $im;
      
   }

   function text($params) {

      $im = $this->image;

      //get color from web colors
      $params['color'] = $this->hex2rgb($params['color'], $im);

      //set x and y coordinates if auto is given or according to corners
      $bbox = imagettfbbox($params['font-size'], 0, $params['font-family'], $params['text']);
      $textWidth = $bbox[4] - $bbox[6];
      $textHeight = $bbox[1] - $bbox[7];
      if ($params['posX'] === "auto") {
         $params['posX'] = ( $this->getWidth() - $textWidth ) / 2;
      } else {
         if (isset($params['corner']) && $params['corner'] != "top-left") {
            //Modify position parameters according to corner
            if ($params['corner'] == "top-right") {
               $params['posX'] = $this->getWidth() - $textWidth - $params['posX'];
            }

           if ($params['corner'] == "bottom-right") {
               $params['posX'] = $this->getWidth() - $textWidth - $params['posX'];
            }
         }

      }

      if ($params['posY'] === "auto") {
         $params['posY'] = ( $this->getHeight() ) / 2;
      } else {
         if (isset($params['corner'])) {
            if ($params['corner'] == "top-left") {
               $params['posY'] = $params['posY'] + $textHeight;
            }

            if ($params['corner'] == "top-right") {
               $params['posY'] = $params['posY'] + $textHeight;
            }

            if ($params['corner'] == "bottom-left") {
               $params['posY'] = $this->getHeight() - $params['posY'];
            }

            if ($params['corner'] == "bottom-right") {
               $params['posY'] = $this->getHeight() - $params['posY'];
            }
         }
      }

      //Print the text
      imagettftext($im, $params['font-size'], 0, $params["posX"], $params["posY"], $params['color'], $params['font-family'], $params['text']);
      $this->image = $im;
   }

   function hex2rgb($hex, $img) {
      $hex = str_replace("#", "", $hex);

      if(strlen($hex) == 3) {
         $r = hexdec(substr($hex,0,1).substr($hex,0,1));
         $g = hexdec(substr($hex,1,1).substr($hex,1,1));
         $b = hexdec(substr($hex,2,1).substr($hex,2,1));
      } else {
         $r = hexdec(substr($hex,0,2));
         $g = hexdec(substr($hex,2,2));
         $b = hexdec(substr($hex,4,2));
      }
      $rgb = imagecolorallocate($img, $r, $g, $b);
      //return implode(",", $rgb); // returns the rgb values separated by commas
      return $rgb; // returns an array with the rgb values
   }
 
}
?>