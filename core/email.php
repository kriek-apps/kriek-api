<?php

Class Email extends Api{
	
	function Send($email = array(),$API = true){
		if (empty($email)){
			global $params;
			$email['from'] = $params['from'];
			$email['to'] = $params['to'];
			$email['subject'] = $params['subject'];
			$email['html'] = $params['html'];
		}
		
		require_once "lib/swift/lib/swift_required.php";

		try {
			$username = $this->config['smtp_user'];
			$password = $this->config['smtp_pass'];
			$transport = Swift_SmtpTransport::newInstance($this->config['smtp_server'], $this->config['smtp_port']);
			$transport->setUsername($username);
			$transport->setPassword($password);
			$swift = Swift_Mailer::newInstance($transport);
			$message = new Swift_Message($email['subject']);
			$message->setFrom($email['from']);
			$message->setBody($email['html'], 'text/html');
			$message->setTo($email['to']);
			$recipients = $swift->send($message, $failures);
			if($API){
				$this->return_json($recipients);
			} else {
				return $recipients;
			}
		} catch (Exception $e) {
			$this->return_error($e->getMessage());
		}
	}

}