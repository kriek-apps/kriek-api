<?php
/* KriekApps API Amazon Module */
// Include the SDK using the Composer autoloader
require 'vendor/autoload.php';

use Aws\S3\S3Client;

class Amazon extends Api{

	function uploadBucket($attr){
		global $params;
		$attr['Key'] = "/" . $params['app_id'] . "/" . $attr['Key'];

		if($attr['ContentEncoding'] == 'gzip') {
			$attr['Body'] = gzencode($attr['Body'],9);
		}

		if(!isset($attr['ACL'])) {
			$attr['ACL'] = 'public-read';
		}

		$client = S3Client::factory(array(
		    'key'    => AMAZON_KEY,
		    'secret' => AMAZON_SECRET,
		    'region' => AMAZON_REGION
		));

		$attr['Bucket'] = AMAZON_BUCKET;

		$result = $client->putObject($attr);
		return $result['ObjectURL'];
	}

	function uploadAppData($file,$data){
		$data = "define(".json_encode($data,JSON_NUMERIC_CHECK).");";

		$this->uploadBucket(array(
		    'Key' => "data/" . $file . ".json",
		    'Body' => $data,
		    'ContentType' => 'application/json',
		    'ContentEncoding' => 'gzip'
		));
	}


	function updateAppData(){
		global $params;
		global $app_data;

		if(!isset($params['app_id'])) {
			$params['app_id'] = $params['id'];
		}

		$config = $params['config']['public'];
		$lang = $params['lang'];

		if(!isset($lang)) {
			$lang = $params['language'];
		}

		$briefcase = $params['briefcase'];

		$this->cleanAppData($config);
		$config['app_id'] = $params['app_id'];

		// TODO APP NAME
		$config['app_name'] = $params['name'];

		// $briefcase = json_decode($briefcase,TRUE);


		for($i = 0; $i < count($briefcase); $i++){
			if($briefcase[$i]['access'] != "private"){
				$tmp[$briefcase[$i]['shorthand']] = $briefcase[$i]['contents'];
			}
		}


		$this->uploadAppData("language",$lang);
		$this->uploadAppData("config",$config);
		$this->uploadAppData("briefcase",$tmp);

		//$this->writeRedirect();
	}

	function cleanAppData(&$data){
		foreach ($data as &$field) {
			if(isset($field['value'])){
				$field = $field['value'];
			} else {
				if(is_array($field)){
					$this->cleanAppData($field);
				}
			}
		}
	}

	function writeRedirect(){
		global $app_data;
		global $params;

		if(!isset($app_data['config']['public']['app_fb_page_url']['value'])) {
			$app_data['config']['public']['app_fb_page_url']['value'] = $params['config']['public']['app_fb_page_url']['value'];
		}

		$file='<script type="text/javascript" src="../js/vendor/json2.min.js"></script><script>var params = window.location.search.replace("?", "").split("&");var obj = {};for (var i = 0; i < params.length; i++) {var pair = params[i].split("=");obj[pair[0]] = pair[1];}window.location = "url here?app_data=" + JSON.stringify(obj); </script>';

		$file = str_replace('url here', $app_data['config']['public']['app_fb_page_url']['value'], $file);

		$this->uploadBucket(array(
		    'Key' => "redirect/index.html",
		    'Body' => $file,
		    'ContentType' => 'text/html'
		));
	}

}